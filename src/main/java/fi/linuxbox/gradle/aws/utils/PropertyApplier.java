package fi.linuxbox.gradle.aws.utils;

import org.gradle.api.Transformer;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;

import java.util.function.Consumer;

/**
 * A utility class for setting properties.
 */
public final class PropertyApplier {
    private final ProviderFactory properties;

    /**
     * Designated constructor.
     *
     * @param properties A factory for creating instances of {@link Provider}.
     */
    public PropertyApplier(final ProviderFactory properties) {
        this.properties = properties;
    }

    /**
     * Applies the given function to the Gradle property value, if the property
     * is set.
     *
     * @param property The name of the Gradle property
     * @param target Where to set the value if the property is defined
     */
    public void gradleProperty(final String property,
                               final Consumer<String> target) {
        gradleProperty(property, this::identity, target);
    }

    /**
     * Applies the given functions to the Gradle property value, if the property
     * is set.
     *
     * @param property The name of the Gradle property
     * @param converter Conversion to apply on the string value of the property
     * @param target Where to set the converted value if the property is defined
     * @param <T> The result type of the conversion
     */
    public <T> void gradleProperty(final String property,
                                   final Transformer<T, String> converter,
                                   final Consumer<T> target) {
        set(properties.gradleProperty(property), converter, target);
    }

    /**
     * Applies the given function to the system property value, if the property
     * is set.
     *
     * @param property The name of the system property
     * @param target Where to set the value if the property is defined
     */
    public void systemProperty(final String property,
                               final Consumer<String> target) {
        systemProperty(property, this::identity, target);
    }

    /**
     * Applies the given functions to the system property value, if the property
     * is set.
     *
     * @param property The name of the system property
     * @param converter Conversion to apply on the string value of the property
     * @param target Where to set the converted value if the property is defined
     * @param <T> The result type of the conversion
     */
    public <T> void systemProperty(final String property,
                                   final Transformer<T, String> converter,
                                   final Consumer<T> target) {
        set(properties.systemProperty(property), converter, target);
    }

    /**
     * Applies the given function to the Gradle or system property value,
     * if either property is set.
     *
     * @param property The name of the Gradle or system property
     * @param target Where to set the value if the property is defined
     */
    public void gradleOrSystemProperty(final String property,
                                       final Consumer<String> target) {
        gradleOrSystemProperty(property, this::identity, target);
    }

    /**
     * Applies the given functions to the Gradle or system property value,
     * if either property is set.
     *
     * @param property The name of the Gradle or system property
     * @param converter Conversion to apply on the string value of the property
     * @param target Where to set the converted value if the property is defined
     * @param <T> The result type of the conversion
     */
    public <T> void gradleOrSystemProperty(final String property,
                                           final Transformer<T, String> converter,
                                           final Consumer<T> target) {
        final Provider<String> gradleProperty = properties.gradleProperty(property);
        final Provider<String> systemProperty = properties.systemProperty(property);
        set(gradleProperty.orElse(systemProperty), converter, target);
    }

    private <T> void set(final Provider<String> property,
                         final Transformer<T, String> converter,
                         final Consumer<T> target) {
        if (property.isPresent()) {
            target.accept(property.map(converter).get());
        }
    }

    private String identity(final String value) {
        return value;
    }
}
