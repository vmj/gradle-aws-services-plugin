package fi.linuxbox.gradle.aws.http.urlconnection;

import fi.linuxbox.gradle.aws.utils.DurationParser;
import fi.linuxbox.gradle.aws.utils.PropertyApplier;
import fi.linuxbox.gradle.aws.http.SdkHttpClientService;
import org.gradle.api.NonNullApi;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import software.amazon.awssdk.http.SdkHttpClient;
import software.amazon.awssdk.http.urlconnection.ProxyConfiguration;
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient;

import javax.inject.Inject;
import java.time.Duration;
import java.util.function.Supplier;

/**
 * Factory class for UrlConnection-based HTTP client.
 * <p>
 * This configures the client from Gradle and system properties.
 * </p>
 * <p>
 *     This is not typically used directly, but via
 *     {@link SdkHttpClientService}.
 * </p>
 */
@NonNullApi
abstract public class UrlConnectionHttpClientFactory implements Supplier<SdkHttpClient> {
    /**
     * Gradle and system property name prefix for UrlConnection related
     * settings.
     */
    public final static String URL_CONNECTION_PREFIX = "software.amazon.awssdk.http.urlconnection.";

    private final static String CONNECTION_TIMEOUT = URL_CONNECTION_PREFIX + "connection.timeout";
    private final static String SOCKET_TIMEOUT = URL_CONNECTION_PREFIX + "socket.timeout";

    private final PropertyApplier urlConnectionProperties;

    /**
     * Injectable constructor.
     *
     * @param providers A factory for creating instances of {@link Provider}.
     */
    @Inject
    public UrlConnectionHttpClientFactory(ProviderFactory providers) {
        urlConnectionProperties = new PropertyApplier(providers);
    }

    /**
     * Factory method for UrlConnection-based HTTP client.
     * <p>
     * The following Gradle and system properties are considered:
     * </p>
     * <dl>
     *     <dt>{@value CONNECTION_TIMEOUT}</dt>
     *     <dd>
     *     Sets the {@link UrlConnectionHttpClient.Builder#connectionTimeout(Duration) connection timeout}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value SOCKET_TIMEOUT}</dt>
     *     <dd>
     *     Sets the {@link UrlConnectionHttpClient.Builder#socketTimeout(Duration) socket timeout}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     * </dl>
     *
     * @return An {@link SdkHttpClient} based on {@link UrlConnectionHttpClient}.
     */
    @Override
    public SdkHttpClient get() {
        final UrlConnectionHttpClient.Builder builder = UrlConnectionHttpClient.builder();
        final ProxyConfiguration.Builder proxy = ProxyConfiguration.builder();

        urlConnectionProperties.gradleOrSystemProperty(CONNECTION_TIMEOUT,
                                                       DurationParser::parse,
                                                       builder::connectionTimeout);
        urlConnectionProperties.gradleOrSystemProperty(SOCKET_TIMEOUT,
                                                       DurationParser::parse,
                                                       builder::socketTimeout);

        builder.proxyConfiguration(proxy.build());
        return builder.build();
    }

}
