package fi.linuxbox.gradle.aws.auth.credentials

import fi.linuxbox.gradle.GradleSpecification
import spock.util.environment.RestoreSystemProperties

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class GradlePropertyCredentialsProviderSpec extends GradleSpecification {

    @RestoreSystemProperties
    void 'AwsCredentialsProviderService provides AWS credentials via GradlePropertyCredentialsProvider (Gradle #gradleVersion)'() {
        given: 'one of the places where DefaultCredentialsProvider looks for the credentials'
        System.setProperty('aws.accessKeyId', 'accessKeyIdFromSystemProperty')
        System.setProperty('aws.secretAccessKey', 'secretAccessKeyFromSystemProperty')

        and: 'the project overrides them using one of the ways Gradle properties can be set'
        gradleProperties << '''
        aws.accessKeyId = expectedAccessKeyId
        aws.secretAccessKey = expectedSecretAccessKey
        '''

        and: 'the project has a custom task that prints the resolved credentials'
        buildScript << '''
        // tag::example[] // @start region=javadoc-example 
        plugins {
            id 'fi.linuxbox.awssdk' version '{gradle-project-version}'
        }

        import fi.linuxbox.gradle.aws.auth.credentials.AwsCredentialsProviderService

        final buildService = AwsCredentialsProviderService.register(project.gradle.sharedServices) //<1>

        tasks.register('printCredentials') {
            usesService buildService //<2>

            doLast {
                final cp = buildService.get().awsCredentialsProvider // <3>
                final credentials = cp.resolveCredentials()
                println credentials.accessKeyId()
                println credentials.secretAccessKey()
            }
        }
        // end::example[] // @end region=javadoc-example
        '''

        when: 'the custom task is executed'
        final result = gradle(gradleVersion, 'printCredentials').build()

        then: 'it succeeds and prints the expected credentials'
        result.task(':printCredentials').outcome == SUCCESS
        result.output.contains 'expectedAccessKeyId'
        result.output.contains 'expectedSecretAccessKey'

        where:
        gradleVersion << gradleVersions
    }
}
