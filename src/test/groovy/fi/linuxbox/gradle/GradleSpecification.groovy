package fi.linuxbox.gradle

import org.gradle.testkit.runner.GradleRunner
import spock.lang.Specification
import spock.lang.TempDir

import java.nio.file.Path

abstract class GradleSpecification extends Specification {
    static final Set<String> gradleVersions = [
            // https://gradle.org/releases/
            // tag::readme-gradle-versions[]
            '8.5',   // Nov 29, 2023
            '8.4',   // Oct 04, 2023
            '8.3',   // Aug 17, 2023
            '8.2.1', // Jul 10, 2023
            '8.1.1', // Apr 21, 2023
            '8.0.2', // Mar 03, 2023
            '7.6.3', // Oct 04, 2023
            '7.5.1', // Aug 05, 2022
            '7.4.2', // Mar 31, 2022
            '7.3.3', // Dec 22, 2021
            '7.2',   // Aug 17, 2021
            '7.1.1', // Jul 02, 2021
            '7.0.2', // May 14, 2021
            '6.9.4', // Feb 22, 2023
            '6.8.3', // Feb 22, 2021
            '6.7.1', // Nov 16, 2020
            '6.6.1', // Aug 25, 2020
            '6.5.1', // Jun 30, 2020
            '6.4.1', // May 15, 2020
            '6.3',   // Mar 24, 2020
            '6.2.2', // Mar 04, 2020
            '6.2',   // Feb 17, 2020
            // end::readme-gradle-versions[]
//            '6.1.1', // Jan 24, 2020 // "no service of type ProviderFactory"
//            '6.0.1', // Nov 18, 2019
//            '5.6.4', // Nov 01, 2019
//            '5.5.1', // Jul 10, 2019
//            '5.4.1', // Apr 26, 2019
//            '5.3.1', // Mar 28, 2019
//            '5.2.1', // Feb 08, 2019
//            '5.1.1', // Jan 10, 2019
//            '5.0',    // Nov 26, 2018
//            '4.10.3', // Dec 05, 2018
//            '4.9',    // Jul 16, 2018
//            '4.8.1',  // Jun 21, 2018
//            '4.7',    // Apr 18, 2018
//            '4.6',    // Feb 28, 2018
//            '4.5.1',  // Feb 05, 2018
//            '4.4.1',  // Dec 20, 2017
//            '4.3.1',  // Nov 08, 2017
//            '4.2.1',  // Oct 02, 2017
//            '4.1',    // Aug 07, 2017
//            '4.0.2',  // Jul 26, 2017
//            '3.5.1',  // Jun 16, 2017
//            '3.4.1',  // Mar 03, 2017
//            '3.3',    // Jan 03, 2017
//            '3.2.1',  // Nov 22, 2016
//            '3.1',    // Sep 19, 2016
//            '3.0',    // Aug 15, 2016
    ]

    static final String latestGradleVersion = gradleVersions.first()
    static final String oldestGradleVersion = gradleVersions.last()

    @TempDir
    Path testProjectDir
    @TempDir
    File testKitDir

    GradleRunner gradle(String gradleVersion, String... args) {
        GradleRunner
                .create()
                .withGradleVersion(gradleVersion)
                .withProjectDir(projectDir)
                .withTestKitDir(testKitDir)
                .withPluginClasspath()
                .withArguments(args)
    }

    File getProjectDir() {
        projectFile(null)
    }

    File getBuildScript() {
        projectFile('build.gradle')
    }

    File getSettingsScript() {
        projectFile('settings.gradle')
    }

    File getGradleProperties() {
        projectFile('gradle.properties')
    }

    File projectFile(String path) {
        if (path == null)
            return testProjectDir.toFile()

        final file = path.split(/\//)
                         .inject(testProjectDir,  { Path a, String b ->
                             a.resolve(b)
                         })
                         .toFile()
        file.parentFile.mkdirs()
        file
    }

}
