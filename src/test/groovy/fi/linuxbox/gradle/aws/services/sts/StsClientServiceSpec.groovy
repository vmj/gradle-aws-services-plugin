package fi.linuxbox.gradle.aws.services.sts

import fi.linuxbox.gradle.GradleSpecification
import org.gradle.tooling.GradleConnector


class StsClientServiceSpec extends GradleSpecification {

    void 'it provides synchronous STS client'() {
        given:
        settingsScript << """
        pluginManagement {
            includeBuild '${System.getProperty('projectUnderTestDir')}'
        }
        """

        and:
        gradleProperties << '''
        # @start region=region-and-credentials-properties
        aws.region = eu-north-1
        aws.accessKeyId = AKIAIOSFODNN7EXAMPLE
        aws.secretAccessKey = wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
        # @end region=region-and-credentials-properties
        '''

        and: 'a custom task that prints the current AWS account ID'
        buildScript << '''
        // @start region=javadoc-example
        buildscript {
            dependencies {
                classpath 'software.amazon.awssdk:sts'
            }
        }
        plugins {
            id 'fi.linuxbox.awssdk' version '{gradle-project-version}'
        }
        import fi.linuxbox.gradle.aws.services.sts.StsClientService

        abstract class CustomTask extends DefaultTask {
            @Internal
            abstract Property<StsClientService> getStsClientService() //<1>

            @TaskAction
            void act() {
                final sts = stsClientService.get().stsClient //<5>
                Objects.requireNonNull(sts)
                // do something with the StsClient
                println "STS Client: OK"
            }
        }

        project.tasks.register('test', CustomTask) {
            final stsClientServiceProvider = StsClientService.register(project.gradle.sharedServices) //<2>
            stsClientService = stsClientServiceProvider //<3>
            usesService stsClientServiceProvider //<4>
        }
        // @end region=javadoc-example
        '''

        and:
        final project = GradleConnector.newConnector()
                                       .forProjectDirectory(projectDir)
                                       .connect()

        and:
        final console = new ByteArrayOutputStream()

        and:
        final task = project.newBuild()
                            .setStandardOutput(console)
                            .withArguments('-q', '--console=plain')
                            .forTasks('test')

        when:
        task.run()

        then:
        console.toString() == 'STS Client: OK\n'
    }

    void 'readme example'() {
        given:
        settingsScript << """
        pluginManagement {
            includeBuild '${System.getProperty('projectUnderTestDir')}'
        }
        """

        and:
        gradleProperties << '''
        aws.region = eu-north-1
        aws.accessKeyId = AKIAIOSFODNN7EXAMPLE
        aws.secretAccessKey = wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
        '''

        and: 'a custom task that prints the current AWS account ID'
        buildScript << '''
        buildscript {
            dependencies {
                classpath 'software.amazon.awssdk:sts'
            }
        }
        plugins {
            id 'fi.linuxbox.awssdk' version '{gradle-project-version}'
        }

        // tag::readme-example-build-gradle[]
        // tag::readme-example-custom-type[]
        import fi.linuxbox.gradle.aws.services.sts.StsClientService
        // end::readme-example-build-gradle[]
        import software.amazon.awssdk.services.sts.StsClient;

        abstract class MyTask extends DefaultTask {
            @Internal
            abstract Property<StsClientService> getStsProvider() //<1>

            @TaskAction
            void act() {
                StsClient sts = getStsProvider().get().getStsClient() //<2>
                Objects.requireNonNull(sts)
                // do something with the StsClient
                println "STS Client: OK"
            }
        }
        // end::readme-example-custom-type[]
        // tag::readme-example-build-gradle[]

        project.tasks.register('myTask', MyTask) {
            final stsServiceProvider = StsClientService.register(project.gradle.sharedServices) //<1>
            stsProvider = stsServiceProvider //<2>
            usesService stsServiceProvider //<3>
        }
        // end::readme-example-build-gradle[]
        '''

        and:
        final project = GradleConnector.newConnector()
                                       .forProjectDirectory(projectDir)
                                       .connect()

        and:
        final console = new ByteArrayOutputStream()

        and:
        final task = project.newBuild()
                            .setStandardOutput(console)
                            .withArguments('-q', '--console=plain')
                            .forTasks('myTask')

        when:
        task.run()

        then:
        console.toString() == 'STS Client: OK\n'
    }

}
