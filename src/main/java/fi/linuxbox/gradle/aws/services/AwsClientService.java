package fi.linuxbox.gradle.aws.services;

import fi.linuxbox.gradle.aws.auth.credentials.AwsCredentialsProviderService;
import fi.linuxbox.gradle.aws.regions.providers.AwsRegionProviderService;
import org.gradle.api.NonNullApi;
import org.gradle.api.tasks.Nested;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.awscore.client.builder.AwsClientBuilder;
import software.amazon.awssdk.utils.SdkAutoCloseable;

/**
 * Base class for build services for AWS SDK clients.
 * <p>
 *     After {@link #createBuilder() creating} the AWS SDK client builder,
 *     this {@link #builder() configures} it using the settings common to every
 *     client builder.
 * </p>
 * @param <B> Type of the AWS SDK client builder
 * @param <C> Type of the AWS SDK client
 */
@NonNullApi
public abstract class AwsClientService<B extends AwsClientBuilder<B, C>, C extends SdkAutoCloseable> implements AutoCloseable {
    private static final Logger log = LoggerFactory.getLogger(AwsClientService.class);

    private final String name;

    /**
     * The shared instance of the AWS SDK client.
     */
    protected final C client;

    /**
     * Constructor.
     *
     * @param name The name to show while logging the client lifecycle
     */
    protected AwsClientService(final String name) {
        this.name = name;
        this.client = client();
    }

    /**
     * Concrete subclass responsibility.
     * <p>
     *     Instantiate the correct {@link AwsClientBuilder AWS SDK client builder}.
     * </p>
     *
     * @return The client builder
     */
    protected abstract B createBuilder();

    /**
     * Configures the AWS SDK client builder.
     * <p>
     *     Concrete subclasses, if overriding this method, should base call.
     * </p>
     * <p>
     *     After {@link #createBuilder() acquiring} the AWS SDK client builder,
     *     this sets the required and optional configuration common to all
     *     client builders:
     * </p>
     * <ul>
     *     <li>The AWS region is resolved using the {@link AwsRegionProviderService}</li>
     *     <li>The AWS credentials are resolved using the {@link AwsCredentialsProviderService}</li>
     * </ul>
     * @return The configured client builder
     */
    protected B builder() {
        return createBuilder()
            // SdkClientBuilder
//          .endpointOverride(URI.create("")) // TODO
            // AwsClientBuilder
            .region(getAwsRegionProviderService().getAwsRegionProvider().getRegion())
            .credentialsProvider(getAwsCredentialsProviderService().getAwsCredentialsProvider())
//          .defaultsMode(DefaultsMode.STANDARD) // TODO
//          .dualstackEnabled(true) // TODO
//          .fipsEnabled(true) // TODO
        ;
    }

    /**
     * Instantiates the AWS SDK client using the  {@link #builder() builder}.
     *
     * @return The configured client
     */
    private C client() {
        log.debug("Building and instantiating AWS SDK client '{}'", name);
        return builder().build();
    }

    /**
     * Closes the AWS SDK client.
     * <p>
     * Gradle invokes this when this build service is no longer needed during a build.
     * </p>
     */
    @Override
    public void close() {
        log.debug("Closing AWS SDK client '{}'", name);
        client.close();
    }

    /**
     * Gradle generated getter for credentials provider.
     *
     * @return the credentials provider
     */
    @Nested
    protected abstract AwsCredentialsProviderService getAwsCredentialsProviderService();

    /**
     * Gradle generated getter for region provider.
     *
     * @return the region provider
     */
    @Nested
    protected abstract AwsRegionProviderService getAwsRegionProviderService();

}
