/**
 * UrlConnection-based HTTP client factory that configures it from Gradle and system properties.
 * <p>
 *     This is not typically used directly, but via
 *     {@link fi.linuxbox.gradle.aws.http.SdkHttpClientService}.
 * </p>
 */
package fi.linuxbox.gradle.aws.http.urlconnection;
