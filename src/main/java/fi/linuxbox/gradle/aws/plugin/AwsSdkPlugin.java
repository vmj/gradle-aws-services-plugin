package fi.linuxbox.gradle.aws.plugin;

import org.gradle.api.NonNullApi;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

/**
 * Implementation class for the {@code fi.linuxbox.awssdk} plugin.
 * <p>
 *     A new instance of this class is created for each project this plugin is
 *     applied to.
 * </p>
 * <p>
 *     This plugin, when applied to a project, does next to nothing.
 *     This plugin is typically used by other plugins, as a library.
 *     Applying this plugin in a build allows build author to customize and
 *     experiment with the
 *     {@linkplain fi.linuxbox.gradle.aws.services build services}
 *     this library provides.
 * </p>
 */
@NonNullApi
public class AwsSdkPlugin implements Plugin<Project> {

    /**
     * Default constructor.
     * <p>
     *     Gradle invokes this whenever this plugin is applied to a project.
     * </p>
     */
    public AwsSdkPlugin() {}

    /**
     * Apply this plugin to the given target {@link Project}.
     *
     * @param project The target {@link Project}
     */
    @Override
    public void apply(Project project) {}

}
