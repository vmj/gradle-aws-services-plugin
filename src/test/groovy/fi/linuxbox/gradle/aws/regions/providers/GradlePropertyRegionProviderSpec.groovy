package fi.linuxbox.gradle.aws.regions.providers

import fi.linuxbox.gradle.GradleSpecification
import spock.util.environment.RestoreSystemProperties

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class GradlePropertyRegionProviderSpec extends GradleSpecification {

    @RestoreSystemProperties
    void 'AwsRegionProviderService provides AWS region via GradlePropertyRegionProvider (Gradle #gradleVersion)'() {
        given: 'one of the places where DefaultAwsRegionProvider looks for the region'
        System.setProperty('aws.region', 'eu-central-1')

        and: 'the project overrides it using one of the ways Gradle properties can be set'
        gradleProperties << '''
        aws.region = eu-north-1
        '''

        and: 'the project has a custom task that prints the resolved region'
        buildScript << '''
        // tag::example[] // @start region=javadoc-example
        plugins {
            id 'fi.linuxbox.awssdk' version '{project_version}'
        }

        import fi.linuxbox.gradle.aws.regions.providers.AwsRegionProviderService

        final buildService = AwsRegionProviderService.register(project.gradle.sharedServices) //<1>

        tasks.register('printRegion') {
            usesService buildService //<2>

            doLast {
                final rp = buildService.get().awsRegionProvider // <3>
                final region = rp.region
                println region.id()
            }
        }
        // end::example[] // @end region=javadoc-example
        '''

        when: 'the custom task is executed'
        final result = gradle(gradleVersion, 'printRegion').build()

        then: 'it succeeds and prints the expected region'
        result.task(':printRegion').outcome == SUCCESS
        result.output.contains 'eu-north-1'

        where:
        gradleVersion << gradleVersions
    }
}
