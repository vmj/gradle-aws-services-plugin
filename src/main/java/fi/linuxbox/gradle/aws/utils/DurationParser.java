package fi.linuxbox.gradle.aws.utils;

import java.time.Duration;
import java.time.format.DateTimeParseException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parser for duration strings.
 */
public final class DurationParser {
    private static final Pattern DURATION_MATCHER = Pattern.compile("^(\\d+)(s|m|h|d|(:?[mn]s))?$");

    private DurationParser() {}

    /**
     * Parses a duration from a string.
     * <p>
     *     Leading and trailing whitespace is ignored.
     * </p>
     * <p>
     *     If the value starts with the letter {@code P}, the value is assumed
     *     to be in ISO-8601 format and is parsed using the
     *     {@link Duration#parse(CharSequence)} method.
     * </p>
     * <p>
     *     Otherwise, the value is expected to be a positive integral number,
     *     followed by one of the supported time units: {@code ns}, {@code ms},
     *     {@code s}, {@code m}, {@code h}, or {@code d} (nanoseconds,
     *     milliseconds, seconds, minutes, hours, and days, respectively).
     * </p>
     * @param object Typically a Gradle or system property value, must be non-{@code null}
     * @return The duration
     */
    public static Duration parse(final String object) {
        Objects.requireNonNull(object);

        final String value = object.trim();
        if (value.startsWith("P")) {
            try {
                return Duration.parse(value);
            } catch (DateTimeParseException e) {
                throw new RuntimeException("invalid ISO-8601 format: '"+value+"'");
            }
        }

        final Matcher matcher = DURATION_MATCHER.matcher(value);
        if (matcher.find()) {
            final String number = matcher.group(1);
            final String unit = matcher.group(2);

            final int amount;
            try {
                amount = Integer.parseInt(number);
            } catch (NumberFormatException e) {
                throw new RuntimeException("invalid duration number: '"+number+"'");
            }
            switch (unit) {
                case "ns":
                    return Duration.ofNanos(amount);
                case "ms":
                    return Duration.ofMillis(amount);
                case "s":
                    return Duration.ofSeconds(amount);
                case "m":
                    return Duration.ofMinutes(amount);
                case "h":
                    return Duration.ofHours(amount);
                case "d":
                    return Duration.ofDays(amount);
                default:
                    throw new RuntimeException("unsupported unit: '"+unit+"'");
            }
        }

        throw new RuntimeException("invalid duration: '"+value+"'");
    }
}
