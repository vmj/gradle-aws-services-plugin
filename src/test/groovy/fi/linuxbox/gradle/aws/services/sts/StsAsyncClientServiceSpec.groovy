package fi.linuxbox.gradle.aws.services.sts

import fi.linuxbox.gradle.GradleSpecification
import org.gradle.tooling.GradleConnector


class StsAsyncClientServiceSpec extends GradleSpecification {

    void 'it provides asynchronous STS client'() {
        given:
        settingsScript << """
        pluginManagement {
            includeBuild '${System.getProperty('projectUnderTestDir')}'
        }
        """

        and:
        gradleProperties << '''
        # @start region=region-and-credentials-properties
        aws.region = eu-north-1
        aws.accessKeyId = AKIAIOSFODNN7EXAMPLE
        aws.secretAccessKey = wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
        # @end region=region-and-credentials-properties
        '''

        and: 'a custom task that prints the current AWS account ID'
        buildScript << '''
        // @start region=javadoc-example
        buildscript {
            dependencies {
                classpath 'software.amazon.awssdk:sts'
            }
        }
        plugins {
            id 'fi.linuxbox.awssdk' version '{gradle-project-version}'
        }
        import fi.linuxbox.gradle.aws.services.sts.StsAsyncClientService

        abstract class CustomTask extends DefaultTask {
            @Internal
            abstract Property<StsAsyncClientService> getStsClientService() //<1>

            @TaskAction
            void act() {
                final sts = stsClientService.get().stsAsyncClient //<5>
                Objects.requireNonNull(sts)
                // do something with the StsClient
                println "STS Client: OK"
            }
        }

        project.tasks.register('test', CustomTask) {
            final stsClientServiceProvider = StsAsyncClientService.register(project.gradle.sharedServices) //<2>
            stsClientService = stsClientServiceProvider //<3>
            usesService stsClientServiceProvider //<4>
        }
        // @end region=javadoc-example
        '''

        and:
        final project = GradleConnector.newConnector()
                                       .forProjectDirectory(projectDir)
                                       .connect()

        and:
        final console = new ByteArrayOutputStream()

        and:
        final task = project.newBuild()
                            .setStandardOutput(console)
                            .withArguments('-q', '--console=plain')
                            .forTasks('test')

        when:
        task.run()

        then:
        console.toString() == 'STS Client: OK\n'
    }


}
