= Controlling the AWS SDK Synchronous HTTP Client Implementation
ifndef::includedir[:includedir: ..]

The AWS SDK has a pluggable synchronous HTTP client.
This document describes how to control which synchronous HTTP client
implementation is used in a build that consumes this project.

NOTE: The choice of synchronous HTTP client implementation should be made by
the build authors, not by plugin authors:  a build can use multiple AWS related
plugins, and they shouldn't step on each other.

== When Used as a Plugin

The plugin defaults to using the Apache-based HTTP client.

For example, if you had the following build script and
executed the `printHttpClientName` task,
you would see `Apache` mentioned in the output.

.Contents of your `build.gradle` file
[source,groovy,subs="attributes+"]
----
include::{includedir}/src/test/groovy/fi/linuxbox/gradle/aws/http/SdkHttpClientSpec.groovy[tag=example,indent=0]
----

If you want to change to the UrlConnection-based client,
you have to do two things.

First, you need to set a Gradle property.

.Contents of your `gradle.properties` file
[source,properties]
----
include::{includedir}/src/test/groovy/fi/linuxbox/gradle/aws/http/SdkHttpClientSpec.groovy[tag=plugin-consumer-gradle-properties,indent=0]
----

Second, you need to add the `url-connection-client` library in the build classpath,
above your plugins block.

.Change in your `build.gradle` file
[source,groovy,subs="attributes+"]
----
include::{includedir}/src/test/groovy/fi/linuxbox/gradle/aws/http/SdkHttpClientSpec.groovy[tag=plugin-consumer-build-gradle,indent=0]
----

With these two changes, if you executed the `printHttpClientName` task,
you would see `UrlConnection` mentioned in the output,
instead of `Apache`.

== When Used as a Library

The library defaults to using Apache-based HTTP client.

For example, if you had the following build script:

.Contents of your `build.gradle` file
[source,groovy,subs="attributes+"]
----
include::{includedir}/src/test/groovy/fi/linuxbox/gradle/aws/http/SdkHttpClientSpec.groovy[tag=library-consumer-build-gradle,indent=0]
----

and the following application code:

.Contents of your `Main.java` file
[source,java]
----
include::{includedir}/src/test/groovy/fi/linuxbox/gradle/aws/http/SdkHttpClientSpec.groovy[tag=library-consumer-main-java,indent=0]
----

and you executed the code,
you would see `Apache` mentioned in the output.

If you want to change to the UrlConnection-based client,
you can change the dependencies:

.Change in your `build.gradle` file
[source,groovy,subs="attributes+"]
----
include::{includedir}/src/test/groovy/fi/linuxbox/gradle/aws/http/SdkHttpClientSpec.groovy[tag=library-consumer-url-connection-dependencies,indent=0]
----

With the dependency changed, if you executed the code,
you would see `UrlConnection` mentioned in the output,
instead of `Apache`.
