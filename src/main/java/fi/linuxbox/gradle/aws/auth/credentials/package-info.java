/**
 * Gradle additions to AWS credentials loading.
 */
package fi.linuxbox.gradle.aws.auth.credentials;
