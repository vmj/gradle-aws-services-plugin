package fi.linuxbox.gradle.aws.auth.credentials;

import org.gradle.api.Action;
import org.gradle.api.NonNullApi;
import org.gradle.api.Project;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import org.gradle.api.services.BuildService;
import org.gradle.api.services.BuildServiceParameters.None;
import org.gradle.api.services.BuildServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProviderChain;
import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider;

import javax.inject.Inject;

/**
 * Provides the {@link AwsCredentialsProvider credentials provider} as a Gradle {@link BuildService build service}.
 * <p>
 *     The credentials are loaded from Gradle and system properties, similar to
 *     all AWS SDKs and AWS CLI.
 *     See the {@link #getAwsCredentialsProvider()} for details.
 * </p>
 * <h2>Example Usage</h2>
 * {@snippet lang=groovy file=fi/linuxbox/gradle/aws/auth/credentials/GradlePropertyCredentialsProviderSpec.groovy region=javadoc-example}
 * <ol>
 *     <li>
 *         During build configuration,
 *         {@link #register(BuildServiceRegistry) register} this build service.
 *         From this registration, you receive the build service provider.
 *     </li>
 *     <li>
 *         Using the {@link org.gradle.api.Task#usesService(Provider) usesService} method,
 *         tell Gradle that the task uses this build service.
 *     </li>
 *     <li>
 *         During task execution, use the {@link Provider#get()} method to
 *         make use of the build service.  The AWS credentials provider will be
 *         created lazily when the {@code get()} method is first called.
 *     </li>
 * </ol>
 */
@NonNullApi
public abstract class AwsCredentialsProviderService implements BuildService<None>, AutoCloseable {
    private static final Logger log = LoggerFactory.getLogger(AwsCredentialsProviderService.class);

    /**
     * A name to use to identify this build service in the {@link BuildServiceRegistry}.
     * <p>
     *     When using the {@link #register(BuildServiceRegistry)} method,
     *     this is the name that is used in the
     *     invocation of {@link BuildServiceRegistry#registerIfAbsent(String, Class, Action)}.
     * </p>
     */
    public static final String NAME = "aws-credentials-provider-service";

    /**
     * Registers this build service, unless already registered.
     * <p>
     * The {@code sharedServices} registry is available using the
     * {@link org.gradle.api.invocation.Gradle#getSharedServices()} method.
     * You can obtain a Gradle instance by calling {@link Project#getGradle()}.
     * </p>
     *
     * @param sharedServices A registry of build services.
     * @return A provider that will create the service instance when queried.
     */
    public static Provider<AwsCredentialsProviderService> register(BuildServiceRegistry sharedServices) {
        log.debug("Registering AWS credentials provider service");
        return sharedServices.registerIfAbsent(
            AwsCredentialsProviderService.NAME,
            AwsCredentialsProviderService.class,
            (spec) -> {}
        );
    }

    /**
     * Returns the credentials provider.
     * <p>
     *     The credentials loading is delegated to
     *     {@link GradlePropertyCredentialsProvider} and
     *     {@link DefaultCredentialsProvider}.  But in summary, the credentials
     *     are loaded in the following order:
     * </p>
     * <ol>
     *      <li>Gradle properties - {@systemProperty aws.accessKeyId}, {@systemProperty aws.secretAccessKey} and {@systemProperty aws.sessionToken}</li>
     *      <li>Java System Properties - {@systemProperty aws.accessKeyId}, {@systemProperty aws.secretAccessKey} and {@systemProperty aws.sessionToken}</li>
     *      <li>Environment Variables - {@code AWS_ACCESS_KEY_ID}, {@code AWS_SECRET_ACCESS_KEY}, and {@code AWS_SESSION_TOKEN}</li>
     *      <li>Web Identity Token credentials from system properties or environment variables</li>
     *      <li>Credential profiles file at the default location ({@code ~/.aws/credentials}) shared by all AWS SDKs and the AWS CLI</li>
     *      <li>Credentials delivered through the Amazon EC2 container service if {@code AWS_CONTAINER_CREDENTIALS_RELATIVE_URI} environment variable is set and security manager has permission to access the variable</li>
     *      <li>Instance profile credentials delivered through the Amazon EC2 metadata service</li>
     * </ol>
     * @return the credentials provider.
     */
    public AwsCredentialsProvider getAwsCredentialsProvider() {
        return awsCredentialsProviderChain;
    }

    /**
     * Injectable constructor.
     * <p>
     *     This is invoked by Gradle when this build service is needed by the
     *     build.
     * </p>
     *
     * @param providers A factory for creating instances of {@link Provider}.
     */
    @Inject
    public AwsCredentialsProviderService(ProviderFactory providers) {
        log.debug("Instantiating AWS credentials provider service");
        awsCredentialsProviderChain = AwsCredentialsProviderChain.of(
            new GradlePropertyCredentialsProvider(providers),
            DefaultCredentialsProvider.create()
        );
    }

    /**
     * Closes the credentials provider.
     * <p>
     * Gradle invokes this when this build service is no longer needed during a build.
     * </p>
     */
    @Override
    public void close() {
        log.debug("Closing AWS credentials provider service");
        awsCredentialsProviderChain.close();
    }

    private final AwsCredentialsProviderChain awsCredentialsProviderChain;
}
