/**
 * Provides AWS SDK Clients as Gradle Build Services.
 */
package fi.linuxbox.gradle.aws.services;
