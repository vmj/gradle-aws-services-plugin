= AWS SDK Clients as Gradle Build Services
:badge_url: https://img.shields.io
:badge_style: ?style=for-the-badge&color=green
ifndef::plugin-id[:plugin-id: fi.linuxbox.awssdk]
ifndef::gradle-project-group[:gradle-project-group: fi.linuxbox.gradle]
ifndef::gradle-project-name[:gradle-project-name: gradle-awssdk]
ifndef::gradle-project-version[:gradle-project-version: 0.4.0]
ifndef::awsSdkVersion[:awsSdkVersion: 2.22.5]
ifndef::apidoc[:apidoc: ./build/docs/javadoc/index.html]
ifndef::testdoc[:testdoc: ./build/reports/tests/test/index.html]
ifndef::license[:license: LICENCE]

image:{badge_url}/gradle-plugin-portal/v/{plugin-id}{badge_style}["Version at Gradle plugin portal",link=https://plugins.gradle.org/plugin/{plugin-id}]
image:{badge_url}/maven-central/v/{gradle-project-group}/{gradle-project-name}{badge_style}["Version at Maven Central", link="https://search.maven.org/artifact/{gradle-project-group}/{gradle-project-name}"]
image:{badge_url}/gitlab/pipeline-status/vmj/{gradle-project-name}{badge_style}["Gitlab pipeline status", link="https://gitlab.com/vmj/{gradle-project-name}/-/pipelines"]

This Java library and Gradle plugin allows Gradle plugin and build authors to
use AWS SDK clients as Gradle build services.

The AWS SDK is quite flexible.
For example, the SDK clients can get the authenticate tokens from multiple places.
The SDK also has a pluggable HTTP implementation.

When your build or plugin obtains the AWS SDK clients using this library,
your build or plugin will retain that flexibility.

== Usage

This project can be consumed either as a Gradle plugin, or as a Maven dependency,
depending on what you are building.

=== Usage as a Library

If you're building a Gradle plugin that is using AWS SDK clients, you
might want to use this project as a dependency.

[source,groovy,subs="attributes+"]
----
repositories {
    mavenCentral()
}
dependencies {
    implementation '{gradle-project-group}:{gradle-project-name}:{gradle-project-version}'
̋}
----

Then you would start <<_using_the_build_services, using the build services>>.

=== Usage as a Plugin

If you're a build author and want to quickly use some AWS SDK client in your
build, you can apply this project as a Gradle plugin.

[source,groovy,subs="attributes+"]
----
plugins {
    id '{plugin-id}' version '{gradle-project-version}'
}
----

Then you would start <<_using_the_build_services, using the build services>>.

[#_using_the_build_services]
=== Using the Build Services

Regardless of the way you are consuming this project,
all the build services provided by this library follow the same pattern
illustrated in the following example.

Say you have a custom Gradle task that needs to talk to AWS Security Token Service (STS).
So you want to have an instance of `StsClient` in your task.
Your task might look like this:

.Using the `StsClientService` (the build service) to get access to an instance of `StsClient` (the AWS client)
[source,groovy,subs="attributes+"]
----
include::./src/test/groovy/fi/linuxbox/gradle/aws/services/sts/StsClientServiceSpec.groovy[tags=readme-example-custom-type,indent=0]
----
<1> Your custom task should have an `@Internal` annotated `abstract Property` for the build service.
    Gradle will implement that method for you, hence the `abstract` keyword.
    Also, build services cannot be task inputs, so the `@Internal` annotation
    is needed.
<2> During task execution, your task would use the `get()` method of the `Property`
    to obtain the instance of the build service.  Gradle will instantiate the
    build service when the first task asks for it, and cache the build service
    for the duration of the build.

To connect the build service to the custom task, a few things need to happen.
The following code could be in your plugin class or in your `build.gradle` script.

[source,groovy,subs="attributes+"]
----
include::./src/test/groovy/fi/linuxbox/gradle/aws/services/sts/StsClientServiceSpec.groovy[tags=readme-example-build-gradle,indent=0]
----
<1> During configuration of the build, you use the `register` method of the build
    service class.  Each build service has one, and they return a `Provider`
    for the build service.
<2> The provider needs to be connected to the task property.
<3> And Gradle needs to be made aware of the connection.

Now, if you executed `myTask`, you should see `STS Client: OK` printed in the
console.

== Documentation

The various build services are documented in the link:{apidoc}[API documentation].

The AWS SDK version that is used is {awsSdkVersion}.
If you need to change the version of the SDK,
see xref:./docs/sdk-version.adoc[Controlling the AWS SDK Version].

The default synchronous HTTP client is Apache-based.
If you need to change the synchronous HTTP client used by the SDK,
see xref:./docs/sdk-http-client-implementation.adoc[Controlling the AWS SDK Synchronous HTTP Client Implementation]

The default asynchronous HTTP client is Netty-based.
If you need to change the asynchronous HTTP client used by the SDK,
see xref:./docs/sdk-async-http-client-implementation.adoc[Controlling the AWS SDK Asynchronous HTTP Client Implementation]

== Test Results

The test results for this release are available in the link:{testdoc}[Test Report].


== License

gradle-awssdk is Free Software, licensed under GNU General Public
License (GPL), version 3 or later. See link:{license}[LICENSE] file for details.
