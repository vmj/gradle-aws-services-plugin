package fi.linuxbox.gradle.aws.http;

import fi.linuxbox.gradle.aws.http.crt.CrtHttpClientFactory;
import fi.linuxbox.gradle.aws.http.netty.NettyHttpClientFactory;
import org.gradle.api.Action;
import org.gradle.api.NonNullApi;
import org.gradle.api.Project;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import org.gradle.api.services.BuildService;
import org.gradle.api.services.BuildServiceParameters;
import org.gradle.api.services.BuildServiceRegistry;
import org.gradle.api.tasks.Nested;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.http.async.SdkAsyncHttpClient;

import javax.inject.Inject;

import java.util.function.Supplier;

import static fi.linuxbox.gradle.aws.http.crt.CrtHttpClientFactory.CRT_PREFIX;
import static fi.linuxbox.gradle.aws.http.netty.NettyHttpClientFactory.NETTY_PREFIX;

/**
 * Provides the {@link SdkAsyncHttpClient asynchronous HTTP client} as a Gradle {@link BuildService build service}.
 * <p>
 *     The default asynchronous HTTP client implementation is Netty-based,
 *     same as with AWS SDK.  One can change the implementation using a Gradle
 *     or system property, similar to the AWS SDK.
 *     See {@link #getSdkAsyncHttpClient()} for details.
 * </p>
 * <p>
 *     Some aspects of the implementation can be configured using Gradle or
 *     system properties.  See {@link NettyHttpClientFactory#get()} and
 *     {@link CrtHttpClientFactory#get()} method documentation
 *     for details.
 * </p>
 * <h2>Example Usage</h2>
 * {@snippet lang=groovy file=fi/linuxbox/gradle/aws/http/SdkAsyncHttpClientSpec.groovy region=javadoc-example}
 * <ol>
 *     <li>Use the {@link #register(BuildServiceRegistry) register} method to
 *          register this build service
 *     </li>
 *     <li>During configuration, remember to tell Gradle that the custom
 *          type is {@link org.gradle.api.Task#usesService(Provider) using the build}
 *          service
 *     </li>
 *     <li>During execution, use the {@link Provider#get()} to lazily instantiate
 *          the build service, and the {@link #getSdkAsyncHttpClient()} getter
 *          to access the HTTP client provider
 *     </li>
 * </ol>
 */
@NonNullApi
abstract public class SdkAsyncHttpClientService implements BuildService<BuildServiceParameters.None>, AutoCloseable {
    private static final Logger log = LoggerFactory.getLogger(SdkAsyncHttpClientService.class);

    /**
     * A name to use to identify this build service in the {@link BuildServiceRegistry}.
     * <p>
     *     When using the {@link #register(BuildServiceRegistry)} method,
     *     this is the name that is used in the
     *     invocation of {@link BuildServiceRegistry#registerIfAbsent(String, Class, Action)}.
     * </p>
     */
    public static final String NAME = "aws-sdk-async-http-client-service";
    /**
     * Gradle and system property name that can be used to choose the
     * asynchronous HTTP client implementation.
     */
    private final static String ASYNC_SERVICE_IMPL = "software.amazon.awssdk.http.async.service.impl";
    /**
     * Value of the {@link #ASYNC_SERVICE_IMPL} system property, when
     * Netty-based HTTP client implementation should be used.
     */
    private final static String NETTY_SDK_ASYNC_HTTP_SERVICE = NETTY_PREFIX + "NettySdkAsyncHttpService";
    /**
     * value of the {@link #ASYNC_SERVICE_IMPL} system property, when
     * CRT-based HTTP client implementation should be used.
     */
    private final static String CRT_SDK_HTTP_SERVICE = CRT_PREFIX + "AwsCrtSdkHttpService";

    /**
     * Registers this build service, unless already registered.
     * <p>
     * The {@code sharedServices} registry is available using the
     * {@link org.gradle.api.invocation.Gradle#getSharedServices()} method.
     * You can obtain a Gradle instance by calling {@link Project#getGradle()}.
     * </p>
     *
     * @param sharedServices A registry of build services.
     * @return A provider that will create the service instance when queried.
     */
    public static Provider<SdkAsyncHttpClientService> register(BuildServiceRegistry sharedServices) {
        log.debug("Registering AWS SDK asynchronous HTTP client service");
        return sharedServices.registerIfAbsent(
            SdkAsyncHttpClientService.NAME,
            SdkAsyncHttpClientService.class,
            (spec) -> {}
        );
    }

    /**
     * Returns the {@link SdkAsyncHttpClient}.
     * <p>
     *     This considers both Gradle and system property
     *     {@value ASYNC_SERVICE_IMPL}.  If both are defined, the Gradle property
     *     takes precedence and the system property is ignored.  If neither is
     *     defined, or the property value is not supported, then the
     *     implementation tries to load the HTTP client implementations in this
     *     order:
     * </p>
     *     <ol>
     *         <li>Netty-based HTTP client</li>
     *         <li>CRT-based HTTP client</li>
     *     </ol>
     * <p>
     *     If the property value is {@value NETTY_SDK_ASYNC_HTTP_SERVICE}, then only
     *     the Netty-based HTTP client will be loaded.  If the property value
     *     is {@value CRT_SDK_HTTP_SERVICE}, then only the
     *     CRT-based HTTP client will be loaded.
     * </p>
     * <p>
     *     Some aspects of the HTTP clients are configurable via Gradle and
     *     system properties.  See {@link NettyHttpClientFactory#get()} and
     *     {@link CrtHttpClientFactory#get()} method documentation
     *     for details.
     * </p>
     * @return the {@link SdkAsyncHttpClient}.
     */
    public SdkAsyncHttpClient getSdkAsyncHttpClient() {
        return sdkAsyncHttpClient;
    }

    /**
     * Injectable constructor.
     * <p>
     *     This is invoked by Gradle when this build service is needed by the
     *     build.
     * </p>
     *
     * @param providers A factory for creating instances of {@link Provider}.
     */
    @Inject
    public SdkAsyncHttpClientService(ProviderFactory providers) {
        log.debug("Instantiating AWS SDK asynchronous HTTP client");
        sdkAsyncHttpClient = createSdkAsyncHttpClient(providers);
    }

    /**
     * Close the resources.
     * <p>
     * Gradle invokes this when this build service is no longer needed during a build.
     * </p>
     */
    @Override
    public void close() {
        log.debug("Closing AWS SDK asynchronous HTTP client");
        sdkAsyncHttpClient.close();
    }


    private SdkAsyncHttpClient createSdkAsyncHttpClient(ProviderFactory providers) {
        final Provider<String> gradleProperty = providers.gradleProperty(ASYNC_SERVICE_IMPL);
        final Provider<String> systemProperty = providers.systemProperty(ASYNC_SERVICE_IMPL);

        final String httpServiceImpl = gradleProperty.orElse(systemProperty).getOrNull();

        NoClassDefFoundError nettyCreationException = null;

        if (httpServiceImpl == null) {
            try {
                return createHttpClient(getNettyHttpClient());
            } catch (NoClassDefFoundError err) {
                nettyCreationException = err;
            }
        } else if (httpServiceImpl.equals(NETTY_SDK_ASYNC_HTTP_SERVICE)) {
            return createHttpClient(getNettyHttpClient());
        } else if (!httpServiceImpl.equals(CRT_SDK_HTTP_SERVICE)) {
            log.warn(String.format("%s property '%s' is set to an unsupported value '%s'\n" +
                                       "Supported values are (without the quotes):\n" +
                                       " * for Netty-based HTTP client: '%s'\n" +
                                       " * for CRT-based HTTP client: '%s'",
                                   gradleProperty.isPresent() ? "Gradle" : "system",
                                   ASYNC_SERVICE_IMPL,
                                   httpServiceImpl,
                                   NETTY_SDK_ASYNC_HTTP_SERVICE,
                                   CRT_SDK_HTTP_SERVICE));
            try {
                return createHttpClient(getNettyHttpClient());
            } catch (NoClassDefFoundError err) {
                nettyCreationException = err;
            }
        }
        try {
            return createHttpClient(getCrtHttpClient());
        } catch (NoClassDefFoundError err) {
            if (nettyCreationException != null)
                throw nettyCreationException;
            throw err;
        }
    }

    private SdkAsyncHttpClient createHttpClient(final Supplier<SdkAsyncHttpClient> factory) {
        final SdkAsyncHttpClient httpClient = factory.get();
        log.debug("Using AWS SDK asynchronous HTTP client '{}'", httpClient.clientName());
        return httpClient;
    }

    /**
     * Gradle generated getter for {@link NettyHttpClientFactory}.
     *
     * @return the {@link NettyHttpClientFactory}
     */
    @Nested
    protected abstract NettyHttpClientFactory getNettyHttpClient();

    /**
     * Gradle generated getter for {@link CrtHttpClientFactory}.
     *
     * @return the {@link CrtHttpClientFactory}
     */
    @Nested
    protected abstract CrtHttpClientFactory getCrtHttpClient();

    private final SdkAsyncHttpClient sdkAsyncHttpClient;
}
