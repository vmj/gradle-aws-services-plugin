package fi.linuxbox.gradle.aws.http.apache;

import fi.linuxbox.gradle.aws.utils.DurationParser;
import fi.linuxbox.gradle.aws.utils.PropertyApplier;
import fi.linuxbox.gradle.aws.http.SdkHttpClientService;
import org.gradle.api.NonNullApi;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import software.amazon.awssdk.http.SdkHttpClient;
import software.amazon.awssdk.http.apache.ApacheHttpClient;
import software.amazon.awssdk.http.apache.ProxyConfiguration;

import javax.inject.Inject;
import java.time.Duration;
import java.util.function.Supplier;

/**
 * Factory class for Apache-based HTTP client.
 * <p>
 * This configures the client from Gradle and system properties.
 * </p>
 * <p>
 *     This is not typically used directly, but via
 *     {@link SdkHttpClientService}.
 * </p>
 */
@NonNullApi
abstract public class ApacheHttpClientFactory implements Supplier<SdkHttpClient> {
    /**
     * Gradle and system property name prefix for Apache related
     * settings.
     */
    public final static String APACHE_PREFIX = "software.amazon.awssdk.http.apache.";

    private final static String CONNECTION_ACQUISITION_TIMEOUT = APACHE_PREFIX + "connection.acquisition.timeout";
    private final static String CONNECTION_TIMEOUT = APACHE_PREFIX + "connection.timeout";
    private final static String SOCKET_TIMEOUT = APACHE_PREFIX + "socket.timeout";
    private final static String CONNECTION_TIME_TO_LIVE = APACHE_PREFIX + "connection.time.to.live";
    private final static String CONNECTION_MAX_IDLE_TIME = APACHE_PREFIX + "connection.max.idle.time";
    private final static String USE_IDLE_CONNECTION_REAPER = APACHE_PREFIX + "use.idle.connection.reaper";
    private final static String EXPECT_CONTINUE_ENABLED = APACHE_PREFIX + "expect.continue.enabled";
    private final static String MAX_CONNECTIONS = APACHE_PREFIX + "max.connections";
    private final static String TCP_KEEP_ALIVE = APACHE_PREFIX + "tcp.keep.alive";

    private final PropertyApplier apacheProperties;

    /**
     * Injectable constructor.
     *
     * @param providers A factory for creating instances of {@link Provider}.
     */
    @Inject
    public ApacheHttpClientFactory(ProviderFactory providers) {
        apacheProperties = new PropertyApplier(providers);
    }

    /**
     * Factory method for Apache-based HTTP client.
     * <p>
     * The following Gradle and system properties are considered:
     * </p>
     * <dl>
     *     <dt>{@value CONNECTION_ACQUISITION_TIMEOUT}</dt>
     *     <dd>
     *     Sets the {@link ApacheHttpClient.Builder#connectionAcquisitionTimeout(Duration) connection acquisition timeout}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value CONNECTION_TIMEOUT}</dt>
     *     <dd>
     *     Sets the {@link ApacheHttpClient.Builder#connectionTimeout(Duration) connection timeout}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value SOCKET_TIMEOUT}</dt>
     *     <dd>
     *     Sets the {@link ApacheHttpClient.Builder#socketTimeout(Duration) socket timeout}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value CONNECTION_TIME_TO_LIVE}</dt>
     *     <dd>
     *     Sets the {@link ApacheHttpClient.Builder#connectionTimeToLive(Duration) connection TTL}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value CONNECTION_MAX_IDLE_TIME}</dt>
     *     <dd>
     *     Sets the {@link ApacheHttpClient.Builder#connectionMaxIdleTime(Duration) max connection idle time}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value USE_IDLE_CONNECTION_REAPER}</dt>
     *     <dd>
     *     Sets whether to use the {@link ApacheHttpClient.Builder#useIdleConnectionReaper(Boolean) idle connection reaper}.
     *     See {@link Boolean#valueOf(String) boolean formats} for valid values.
     *     </dd>
     *     <dt>{@value EXPECT_CONTINUE_ENABLED}</dt>
     *     <dd>
     *     Sets whether to use the {@link ApacheHttpClient.Builder#expectContinueEnabled(Boolean) expect-continue handshake}.
     *     See {@link Boolean#valueOf(String) boolean formats} for valid values.
     *     </dd>
     *     <dt>{@value MAX_CONNECTIONS}</dt>
     *     <dd>
     *     Sets the {@link ApacheHttpClient.Builder#maxConnections(Integer) max number of connections}.
     *     See {@link Integer#parseInt(String) int formats} for valid values.
     *     </dd>
     *     <dt>{@value TCP_KEEP_ALIVE}</dt>
     *     <dd>
     *     Sets whether to use the {@link ApacheHttpClient.Builder#tcpKeepAlive(Boolean) TCP KeepAlive}.
     *     See {@link Integer#parseInt(String) int formats} for valid values.
     *     </dd>
     * </dl>
     *
     * @return An {@link SdkHttpClient} based on {@link ApacheHttpClient}.
     */
    @Override
    public SdkHttpClient get() {
        final ApacheHttpClient.Builder builder = ApacheHttpClient.builder();
        final ProxyConfiguration.Builder proxy = ProxyConfiguration.builder();

        apacheProperties.gradleOrSystemProperty(CONNECTION_ACQUISITION_TIMEOUT,
                                                DurationParser::parse,
                                                builder::connectionAcquisitionTimeout);
        apacheProperties.gradleOrSystemProperty(CONNECTION_TIMEOUT,
                                                Duration::parse,
                                                builder::connectionTimeout);
        apacheProperties.gradleOrSystemProperty(SOCKET_TIMEOUT,
                                                DurationParser::parse,
                                                builder::socketTimeout);

        apacheProperties.gradleOrSystemProperty(CONNECTION_TIME_TO_LIVE,
                                                DurationParser::parse,
                                                builder::connectionTimeToLive);
        apacheProperties.gradleOrSystemProperty(CONNECTION_MAX_IDLE_TIME,
                                                DurationParser::parse,
                                                builder::connectionMaxIdleTime);
        apacheProperties.gradleOrSystemProperty(USE_IDLE_CONNECTION_REAPER,
                                                Boolean::valueOf,
                                                builder::useIdleConnectionReaper);

        apacheProperties.gradleOrSystemProperty(EXPECT_CONTINUE_ENABLED,
                                                Boolean::valueOf,
                                                builder::expectContinueEnabled);

        apacheProperties.gradleOrSystemProperty(MAX_CONNECTIONS,
                                                Integer::parseInt,
                                                builder::maxConnections);

        apacheProperties.gradleOrSystemProperty(TCP_KEEP_ALIVE,
                                                Boolean::valueOf,
                                                builder::tcpKeepAlive);

        builder.proxyConfiguration(proxy.build());
        return builder.build();
    }
}
