package fi.linuxbox.gradle.aws.services;

import fi.linuxbox.gradle.aws.http.SdkHttpClientService;
import org.gradle.api.NonNullApi;
import org.gradle.api.tasks.Nested;
import software.amazon.awssdk.awscore.client.builder.AwsClientBuilder;
import software.amazon.awssdk.core.client.builder.SdkSyncClientBuilder;
import software.amazon.awssdk.utils.SdkAutoCloseable;

/**
 * Base class for build services for synchronous AWS SDK clients.
 *
 * @param <B> Type of the asynchronous client builder
 * @param <C> Type of the asynchronous client
 */
@NonNullApi
public abstract class AwsSyncClientService<B extends SdkSyncClientBuilder<B, C> & AwsClientBuilder<B, C>, C extends SdkAutoCloseable> extends AwsClientService<B, C> {

    /**
     * Constructor.
     *
     * @param name The name to show while logging the client lifecycle
     */
    protected AwsSyncClientService(final String name) {
        super(name);
    }

    /**
     * Configures the AWS SDK client builder.
     * <p>
     *     Extends the {@link AwsClientService#builder() base class implementation} by further configuring the AWS SDK client builder:
     * </p>
     * <ul>
     *     <li>The synchronous SDK HTTP client is set using the {@link SdkHttpClientService}</li>
     * </ul>
     * <p>
     *     Concrete subclasses, if overriding this method, should base call.
     * </p>
     * @return The configured client builder
     */
    @Override
    protected B builder() {
        return super.builder()
             .httpClient(getSdkHttpClientService().getSdkHttpClient());
    }

    /**
     * Gradle generated getter for synchronous HTTP client provider.
     *
     * @return the synchronous HTTP client provider
     */
    @Nested
    protected abstract SdkHttpClientService getSdkHttpClientService();

}
