package fi.linuxbox.gradle.aws.auth.credentials;

import org.gradle.api.NonNullApi;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.core.SdkSystemSetting;
import software.amazon.awssdk.core.exception.SdkClientException;

import java.util.Optional;

import static software.amazon.awssdk.utils.StringUtils.isBlank;
import static software.amazon.awssdk.utils.StringUtils.trim;

/**
 * {@link AwsCredentialsProvider} implementation that loads credentials from Gradle properties.
 * <p>
 *     The Gradle property names are {@systemProperty aws.accessKeyId},
 *     {@systemProperty aws.secretAccessKey} and {@systemProperty aws.sessionToken}.
 * </p>
 * <p>
 *     This is used via {@link AwsCredentialsProviderService}.
 * </p>
 */
@NonNullApi
public class GradlePropertyCredentialsProvider implements AwsCredentialsProvider {

    private final ProviderFactory providers;

    /**
     * Constructor.
     * <p>
     *     Invoked by {@link AwsCredentialsProviderService}.
     * </p>
     * @param providers A factory for creating instances of {@link Provider}.
     */
    public GradlePropertyCredentialsProvider(ProviderFactory providers) {
        this.providers = providers;
    }

    @Override
    public AwsCredentials resolveCredentials() {
        String accessKey = trim(loadSetting(SdkSystemSetting.AWS_ACCESS_KEY_ID).orElse(null));
        String secretKey = trim(loadSetting(SdkSystemSetting.AWS_SECRET_ACCESS_KEY).orElse(null));
        String sessionToken = trim(loadSetting(SdkSystemSetting.AWS_SESSION_TOKEN).orElse(null));

        if (isBlank(accessKey)) {
            throw SdkClientException.builder()
                                    .message(String.format("Unable to load credentials from Gradle properties. Access key must be" +
                                                               " specified via Gradle property (%s).",
                                                           SdkSystemSetting.AWS_ACCESS_KEY_ID.property()))
                                    .build();
        }

        if (isBlank(secretKey)) {
            throw SdkClientException.builder()
                                    .message(String.format("Unable to load credentials from Gradle properties. Secret key must be" +
                                                               " specified via Gradle property (%s).",
                                                           SdkSystemSetting.AWS_SECRET_ACCESS_KEY.property()))
                                    .build();
        }

        return isBlank(sessionToken)
            ? AwsBasicCredentials.create(accessKey, secretKey)
            : AwsSessionCredentials.create(accessKey, secretKey, sessionToken);
    }

    private Optional<String> loadSetting(SdkSystemSetting setting) {
        return Optional.ofNullable(
            providers.gradleProperty(setting.property())
                     .getOrNull()
        );
    }
}
