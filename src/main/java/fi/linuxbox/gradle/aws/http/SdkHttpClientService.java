package fi.linuxbox.gradle.aws.http;

import fi.linuxbox.gradle.aws.http.apache.ApacheHttpClientFactory;
import fi.linuxbox.gradle.aws.http.urlconnection.UrlConnectionHttpClientFactory;
import org.gradle.api.Action;
import org.gradle.api.NonNullApi;
import org.gradle.api.Project;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import org.gradle.api.services.BuildService;
import org.gradle.api.services.BuildServiceParameters;
import org.gradle.api.services.BuildServiceRegistry;
import org.gradle.api.tasks.Nested;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.http.SdkHttpClient;

import javax.inject.Inject;

import java.util.function.Supplier;

import static fi.linuxbox.gradle.aws.http.apache.ApacheHttpClientFactory.APACHE_PREFIX;
import static fi.linuxbox.gradle.aws.http.urlconnection.UrlConnectionHttpClientFactory.URL_CONNECTION_PREFIX;

/**
 * Provides the {@link SdkHttpClient synchronous HTTP client} as a Gradle {@link BuildService build service}.
 * <p>
 *     The default synchronous HTTP client implementation is Apache-based,
 *     same as with AWS SDK.  One can change the implementation using a Gradle
 *     or system property, similar to the AWS SDK.
 *     See {@link #getSdkHttpClient()} for details.
 * </p>
 * <p>
 *     Some aspects of the implementation can be configured using Gradle or
 *     system properties.  See {@link ApacheHttpClientFactory#get()} and
 *     {@link UrlConnectionHttpClientFactory#get()} method documentation
 *     for details.
 * </p>
 * <h2>Example Usage</h2>
 * {@snippet lang=groovy file=fi/linuxbox/gradle/aws/http/SdkHttpClientSpec.groovy region=javadoc-example}
 * <ol>
 *     <li>Use the {@link #register(BuildServiceRegistry) register} method to
 *          register this build service
 *     </li>
 *     <li>During configuration, remember to tell Gradle that the custom
 *          type is {@link org.gradle.api.Task#usesService(Provider) using the build}
 *          service
 *     </li>
 *     <li>During execution, use the {@link Provider#get()} to lazily instantiate
 *          the build service, and the {@link #getSdkHttpClient()} getter
 *          to access the HTTP client provider
 *     </li>
 * </ol>
 */
@NonNullApi
abstract public class SdkHttpClientService implements BuildService<BuildServiceParameters.None>, AutoCloseable {
    private static final Logger log = LoggerFactory.getLogger(SdkHttpClientService.class);

    /**
     * A name to use to identify this build service in the {@link BuildServiceRegistry}.
     * <p>
     *     When using the {@link #register(BuildServiceRegistry)} method,
     *     this is the name that is used in the
     *     invocation of {@link BuildServiceRegistry#registerIfAbsent(String, Class, Action)}.
     * </p>
     */
    public static final String NAME = "aws-sdk-http-client-service";
    /**
     * Gradle and system property name that can be used to choose the
     * synchronous HTTP client implementation.
     */
    private final static String HTTP_SERVICE_IMPL = "software.amazon.awssdk.http.service.impl";
    /**
     * Value of the {@link #HTTP_SERVICE_IMPL} system property, when
     * Apache-based HTTP client implementation should be used.
     */
    private final static String APACHE_SDK_HTTP_SERVICE = APACHE_PREFIX + "ApacheSdkHttpService";
    /**
     * value of the {@link #HTTP_SERVICE_IMPL} system property, when
     * UrlConnection-based HTTP client implementation should be used.
     */
    private final static String URL_CONNECTION_SDK_HTTP_SERVICE = URL_CONNECTION_PREFIX + "UrlConnectionSdkHttpService";

    /**
     * Registers this build service, unless already registered.
     * <p>
     * The {@code sharedServices} registry is available using the
     * {@link org.gradle.api.invocation.Gradle#getSharedServices()} method.
     * You can obtain a Gradle instance by calling {@link Project#getGradle()}.
     * </p>
     *
     * @param sharedServices A registry of build services.
     * @return A provider that will create the service instance when queried.
     */
    public static Provider<SdkHttpClientService> register(BuildServiceRegistry sharedServices) {
        log.debug("Registering AWS SDK synchronous HTTP client service");
        return sharedServices.registerIfAbsent(
            SdkHttpClientService.NAME,
            SdkHttpClientService.class,
            (spec) -> {}
        );
    }

    /**
     * Returns the {@link SdkHttpClient}.
     * <p>
     *     This considers both Gradle and system property
     *     {@value HTTP_SERVICE_IMPL}.  If both are defined, the Gradle property
     *     takes precedence and the system property is ignored.  If neither is
     *     defined, or the property value is not supported, then the
     *     implementation tries to load the HTTP client implementations in this
     *     order:
     * </p>
     *     <ol>
     *         <li>Apache-based HTTP client</li>
     *         <li>UrlConnection-based HTTP client</li>
     *     </ol>
     * <p>
     *     If the property value is {@value APACHE_SDK_HTTP_SERVICE}, then only
     *     the Apache-based HTTP client will be loaded.  If the property value
     *     is {@value URL_CONNECTION_SDK_HTTP_SERVICE}, then only the
     *     UrlConnection-based HTTP client will be loaded.
     * </p>
     * <p>
     *     Some aspects of the HTTP clients are configurable via Gradle and
     *     system properties.  See {@link ApacheHttpClientFactory#get()} and
     *     {@link UrlConnectionHttpClientFactory#get()} method documentation
     *     for details.
     * </p>
     * @return the {@link SdkHttpClient}.
     */
    public SdkHttpClient getSdkHttpClient() {
        return sdkHttpClient;
    }

    /**
     * Injectable constructor.
     * <p>
     *     This is invoked by Gradle when this build service is needed by the
     *     build.
     * </p>
     *
     * @param providers A factory for creating instances of {@link Provider}.
     */
    @Inject
    public SdkHttpClientService(ProviderFactory providers) {
        log.debug("Instantiating AWS SDK synchronous HTTP client");
        sdkHttpClient = createSdkHttpClient(providers);
    }

    /**
     * Close the resources.
     * <p>
     * Gradle invokes this when this build service is no longer needed during a build.
     * </p>
     */
    @Override
    public void close() {
        log.debug("Closing AWS SDK synchronous HTTP client");
        sdkHttpClient.close();
    }

    private SdkHttpClient createSdkHttpClient(ProviderFactory providers) {
        final Provider<String> gradleProperty = providers.gradleProperty(HTTP_SERVICE_IMPL);
        final Provider<String> systemProperty = providers.systemProperty(HTTP_SERVICE_IMPL);

        final String httpServiceImpl = gradleProperty.orElse(systemProperty).getOrNull();

        NoClassDefFoundError apacheCreationException = null;

        if (httpServiceImpl == null) {
            try {
                return createHttpClient(getApacheHttpClient());
            } catch (NoClassDefFoundError err) {
                apacheCreationException = err;
            }
        } else if (httpServiceImpl.equals(APACHE_SDK_HTTP_SERVICE)) {
            return createHttpClient(getApacheHttpClient());
        } else if (!httpServiceImpl.equals(URL_CONNECTION_SDK_HTTP_SERVICE)) {
            log.warn(String.format("%s property '%s' is set to an unsupported value '%s'\n" +
                                       "Supported values are (without the quotes):\n" +
                                       " * for Apache-based HTTP client: '%s'\n" +
                                       " * for UrlConnection-based HTTP client: '%s'",
                                   gradleProperty.isPresent() ? "Gradle" : "system",
                                   HTTP_SERVICE_IMPL,
                                   httpServiceImpl,
                                   APACHE_SDK_HTTP_SERVICE,
                                   URL_CONNECTION_SDK_HTTP_SERVICE));
            try {
                return createHttpClient(getApacheHttpClient());
            } catch (NoClassDefFoundError err) {
                apacheCreationException = err;
            }
        }
        try {
            return createHttpClient(getUrlConnectionHttpClient());
        } catch (NoClassDefFoundError err) {
            if (apacheCreationException != null)
                throw apacheCreationException;
            throw err;
        }
    }

    private SdkHttpClient createHttpClient(final Supplier<SdkHttpClient> factory) {
        final SdkHttpClient httpClient = factory.get();
        log.debug("Using AWS SDK synchronous HTTP client '{}'", httpClient.clientName());
        return httpClient;
    }

    /**
     * Gradle generated getter for {@link ApacheHttpClientFactory}.
     *
     * @return the {@link ApacheHttpClientFactory}
     */
    @Nested
    protected abstract ApacheHttpClientFactory getApacheHttpClient();

    /**
     * Gradle generated getter for {@link UrlConnectionHttpClientFactory}.
     *
     * @return the {@link UrlConnectionHttpClientFactory}
     */
    @Nested
    protected abstract UrlConnectionHttpClientFactory getUrlConnectionHttpClient();

    private final SdkHttpClient sdkHttpClient;
}
