package fi.linuxbox.gradle.aws.utils

import spock.lang.Specification

import static fi.linuxbox.gradle.aws.utils.DurationParser.parse
import static java.time.Duration.ofDays
import static java.time.Duration.ofHours
import static java.time.Duration.ofMillis
import static java.time.Duration.ofMinutes
import static java.time.Duration.ofNanos
import static java.time.Duration.ofSeconds

class DurationParserSpec extends Specification {

    void 'it should parse ISO-8601 durations'() {
        expect:
        parse('PT15M') == ofMinutes(15)
    }

    void 'it should parse numbers with known unit'() {
        expect:
        parse(input) == expectedDuration

        where:
        input    || expectedDuration

        '1ns'    || ofNanos(1)
        '12ms'   || ofMillis(12)
        ' 123s'  || ofSeconds(123)
        '1234m ' || ofMinutes(1234)
        '2h'     || ofHours(2)
        '3d'     || ofDays(3)
    }

    void 'it should not accept invalid numbers or units'() {
        when:
        parse(input)

        then:
        final err = thrown(RuntimeException)
        err.message == expectedErrorMessage

        where:
        input  || expectedErrorMessage
        '1ss'  || "invalid duration: '1ss'"
        '1hs'  || "invalid duration: '1hs'"
        '1ds'  || "invalid duration: '1ds'"
        '123y' || "invalid duration: '123y'"
        '.1s'  || "invalid duration: '.1s'"
    }
}
