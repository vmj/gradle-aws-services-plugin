package fi.linuxbox.gradle.aws.auth.credentials


import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification
import spock.util.environment.RestoreSystemProperties

class DefaultCredentialsProviderSpec extends Specification {

    @RestoreSystemProperties
    void 'AwsCredentialsProviderService provides AWS credentials via DefaultCredentialsProvider'() {
        given: 'one of the places where DefaultCredentialsProvider looks for the credentials'
        System.setProperty('aws.accessKeyId', 'expectedAccessKeyId')
        System.setProperty('aws.secretAccessKey', 'expectedSecretAccessKey')

        and: 'the project is using the AwsCredentialsProvider via BuildService'
        final project = ProjectBuilder.builder().build()
        final buildService = AwsCredentialsProviderService.register(project.gradle.sharedServices)

        when: 'some task is resolving the credentials'
        final credentials = buildService.get()
                .awsCredentialsProvider
                .resolveCredentials()

        then: 'the expected credentials are found'
        credentials.accessKeyId() == 'expectedAccessKeyId'
        credentials.secretAccessKey() == 'expectedSecretAccessKey'
    }

}
