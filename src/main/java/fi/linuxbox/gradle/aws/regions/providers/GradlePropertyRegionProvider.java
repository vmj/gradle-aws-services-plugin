package fi.linuxbox.gradle.aws.regions.providers;

import org.gradle.api.NonNullApi;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import software.amazon.awssdk.core.SdkSystemSetting;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.regions.providers.AwsRegionProvider;

import java.util.Optional;

/**
 * {@link AwsRegionProvider} implementation that loads region from Gradle properties.
 * <p>
 *     The Gradle property name is {@systemProperty aws.region}.
 * </p>
 * <p>
 *     This is used via {@link AwsRegionProviderService}.
 * </p>
 */
@NonNullApi
public class GradlePropertyRegionProvider implements AwsRegionProvider {

    private final ProviderFactory providers;

    /**
     * Constructor.
     * <p>
     *     Invoked by {@link AwsRegionProviderService}.
     * </p>
     * @param providers A factory for creating instances of {@link Provider}.
     */
    public GradlePropertyRegionProvider(ProviderFactory providers) {
        this.providers = providers;
    }

    @Override
    public Region getRegion() {
        return loadSetting(SdkSystemSetting.AWS_REGION)
            .map(Region::of)
            .orElseThrow(this::exception);
    }

    private Optional<String> loadSetting(SdkSystemSetting setting) {
        return Optional.ofNullable(
            providers.gradleProperty(setting.property())
                     .getOrNull()
        );
    }

    private SdkClientException exception() {
        return SdkClientException.builder().message(String.format("Unable to load region from Gradle properties. Region" +
                                                                      " must be specified via Gradle property (%s).",
                                                                  SdkSystemSetting.AWS_REGION.property())).build();
    }

}
