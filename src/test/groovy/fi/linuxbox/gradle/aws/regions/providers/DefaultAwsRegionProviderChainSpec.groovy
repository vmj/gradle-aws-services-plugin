package fi.linuxbox.gradle.aws.regions.providers


import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification
import spock.util.environment.RestoreSystemProperties

class DefaultAwsRegionProviderChainSpec extends Specification {

    @RestoreSystemProperties
    void 'AwsRegionProviderService provides AWS region via DefaultAwsRegionProviderChain'() {
        given: 'one of the places where DefaultAwsRegionProviderChain looks for the region'
        System.setProperty('aws.region', 'eu-north-1')

        and: 'the project is using the AwsRegionProviderService via BuildService'
        final project = ProjectBuilder.builder().build()
        final buildService = AwsRegionProviderService.register(project.gradle.sharedServices)

        when: 'some task is resolving the region'
        final region = buildService.get()
                .awsRegionProvider
                .getRegion()

        then: 'the expected credentials are found'
        region.id() == 'eu-north-1'
    }

}
