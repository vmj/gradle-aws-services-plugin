package fi.linuxbox.gradle.aws.http.netty;

import fi.linuxbox.gradle.aws.utils.DurationParser;
import fi.linuxbox.gradle.aws.utils.PropertyApplier;
import fi.linuxbox.gradle.aws.http.SdkAsyncHttpClientService;
import org.gradle.api.NonNullApi;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import software.amazon.awssdk.http.SdkHttpClient;
import software.amazon.awssdk.http.async.SdkAsyncHttpClient;
import software.amazon.awssdk.http.nio.netty.NettyNioAsyncHttpClient;
import software.amazon.awssdk.http.nio.netty.ProxyConfiguration;

import javax.inject.Inject;
import java.time.Duration;
import java.util.function.Supplier;

/**
 * Factory class for Netty-based HTTP client.
 * <p>
 * This configures the client from Gradle and system properties.
 * </p>
 * <p>
 *     This is not typically used directly, but via
 *     {@link SdkAsyncHttpClientService}.
 * </p>
 */
@NonNullApi
abstract public class NettyHttpClientFactory implements Supplier<SdkAsyncHttpClient> {
    /**
     * Gradle and system property name prefix for Netty related
     * settings.
     */
    public final static String NETTY_PREFIX = "software.amazon.awssdk.http.nio.netty.";

    private final static String CONNECTION_ACQUISITION_TIMEOUT = NETTY_PREFIX + "connection.acquisition.timeout";
    private final static String CONNECTION_TIMEOUT = NETTY_PREFIX + "connection.timeout";
    private final static String TLS_NEGOTIATION_TIMEOUT = NETTY_PREFIX + "tls.negotiation.timeout";
    private final static String READ_TIMEOUT = NETTY_PREFIX + "read.timeout";
    private final static String WRITE_TIMEOUT = NETTY_PREFIX + "write.timeout";
    private final static String CONNECTION_TIME_TO_LIVE = NETTY_PREFIX + "connection.time.to.live";
    private final static String CONNECTION_MAX_IDLE_TIME = NETTY_PREFIX + "connection.max.idle.time";
    private final static String USE_IDLE_CONNECTION_REAPER = NETTY_PREFIX + "use.idle.connection.reaper";
    private final static String MAX_PENDING_CONNECTION_ACQUIRES = NETTY_PREFIX + "max.pending.connection.acquires";
    private final static String MAX_CONCURRENCY = NETTY_PREFIX + "max.concurrency";
    private final static String TCP_KEEP_ALIVE = NETTY_PREFIX + "tcp.keep.alive";

    private final PropertyApplier nettyProperties;

    /**
     * Injectable constructor.
     *
     * @param providers A factory for creating instances of {@link Provider}.
     */
    @Inject
    public NettyHttpClientFactory(ProviderFactory providers) {
        nettyProperties = new PropertyApplier(providers);
    }

    /**
     * Factory method for Netty-based HTTP client.
     * <p>
     * The following Gradle and system properties are considered:
     * </p>
     * <dl>
     *     <dt>{@value CONNECTION_ACQUISITION_TIMEOUT}</dt>
     *     <dd>
     *     Sets the {@link NettyNioAsyncHttpClient.Builder#connectionAcquisitionTimeout(Duration) connection acquisition timeout}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value CONNECTION_TIMEOUT}</dt>
     *     <dd>
     *     Sets the {@link NettyNioAsyncHttpClient.Builder#connectionTimeout(Duration) connection timeout}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value TLS_NEGOTIATION_TIMEOUT}</dt>
     *     <dd>
     *     Sets the {@link NettyNioAsyncHttpClient.Builder#tlsNegotiationTimeout(Duration) TLS negotiation timeout}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value READ_TIMEOUT}</dt>
     *     <dd>
     *     Sets the {@link NettyNioAsyncHttpClient.Builder#readTimeout(Duration) read timeout}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value WRITE_TIMEOUT}</dt>
     *     <dd>
     *     Sets the {@link NettyNioAsyncHttpClient.Builder#writeTimeout(Duration) write timeout}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value CONNECTION_TIME_TO_LIVE}</dt>
     *     <dd>
     *     Sets the {@link NettyNioAsyncHttpClient.Builder#connectionTimeToLive(Duration) connection TTL}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value CONNECTION_MAX_IDLE_TIME}</dt>
     *     <dd>
     *     Sets the {@link NettyNioAsyncHttpClient.Builder#connectionMaxIdleTime(Duration) max connection idle time}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value USE_IDLE_CONNECTION_REAPER}</dt>
     *     <dd>
     *     Sets whether to use the {@link NettyNioAsyncHttpClient.Builder#useIdleConnectionReaper(Boolean) idle connection reaper}.
     *     See {@link Boolean#valueOf(String) boolean formats} for valid values.
     *     </dd>
     *     <dt>{@value MAX_PENDING_CONNECTION_ACQUIRES}</dt>
     *     <dd>
     *     Sets the {@link NettyNioAsyncHttpClient.Builder#maxPendingConnectionAcquires(Integer) max number of pending connection acquires}.
     *     See {@link Integer#parseInt(String) int formats} for valid values.
     *     </dd>
     *     <dt>{@value MAX_CONCURRENCY}</dt>
     *     <dd>
     *     Sets the {@link NettyNioAsyncHttpClient.Builder#maxConcurrency(Integer) max number of concurrent requests}.
     *     See {@link Integer#parseInt(String) int formats} for valid values.
     *     </dd>
     *     <dt>{@value TCP_KEEP_ALIVE}</dt>
     *     <dd>
     *     Sets whether to use the {@link NettyNioAsyncHttpClient.Builder#tcpKeepAlive(Boolean) TCP KeepAlive}.
     *     See {@link Integer#parseInt(String) int formats} for valid values.
     *     </dd>
     * </dl>
     *
     * @return An {@link SdkHttpClient} based on {@link NettyNioAsyncHttpClient}.
     */
    @Override
    public SdkAsyncHttpClient get() {
        final NettyNioAsyncHttpClient.Builder builder = NettyNioAsyncHttpClient.builder();
        final ProxyConfiguration.Builder proxy = ProxyConfiguration.builder();

        nettyProperties.gradleOrSystemProperty(CONNECTION_ACQUISITION_TIMEOUT,
                                               DurationParser::parse,
                                               builder::connectionAcquisitionTimeout);
        nettyProperties.gradleOrSystemProperty(CONNECTION_TIMEOUT,
                                               Duration::parse,
                                               builder::connectionTimeout);
        nettyProperties.gradleOrSystemProperty(TLS_NEGOTIATION_TIMEOUT,
                                               DurationParser::parse,
                                               builder::tlsNegotiationTimeout);
        nettyProperties.gradleOrSystemProperty(READ_TIMEOUT,
                                               DurationParser::parse,
                                               builder::readTimeout);
        nettyProperties.gradleOrSystemProperty(WRITE_TIMEOUT,
                                               DurationParser::parse,
                                               builder::writeTimeout);

        nettyProperties.gradleOrSystemProperty(CONNECTION_TIME_TO_LIVE,
                                               DurationParser::parse,
                                               builder::connectionTimeToLive);
        nettyProperties.gradleOrSystemProperty(CONNECTION_MAX_IDLE_TIME,
                                               DurationParser::parse,
                                               builder::connectionMaxIdleTime);
        nettyProperties.gradleOrSystemProperty(USE_IDLE_CONNECTION_REAPER,
                                               Boolean::valueOf,
                                               builder::useIdleConnectionReaper);

        nettyProperties.gradleOrSystemProperty(MAX_PENDING_CONNECTION_ACQUIRES,
                                               Integer::parseInt,
                                               builder::maxPendingConnectionAcquires);
        nettyProperties.gradleOrSystemProperty(MAX_CONCURRENCY,
                                               Integer::parseInt,
                                               builder::maxConcurrency);

        nettyProperties.gradleOrSystemProperty(TCP_KEEP_ALIVE,
                                               Boolean::valueOf,
                                               builder::tcpKeepAlive);

        builder.proxyConfiguration(proxy.build());
        return builder.build();
    }
}
