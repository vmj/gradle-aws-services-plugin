package fi.linuxbox.gradle.aws.regions.providers;

import org.gradle.api.Action;
import org.gradle.api.NonNullApi;
import org.gradle.api.Project;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import org.gradle.api.services.BuildService;
import org.gradle.api.services.BuildServiceParameters.None;
import org.gradle.api.services.BuildServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.regions.providers.AwsRegionProvider;
import software.amazon.awssdk.regions.providers.AwsRegionProviderChain;
import software.amazon.awssdk.regions.providers.DefaultAwsRegionProviderChain;

import javax.inject.Inject;

/**
 * Provides the {@link AwsRegionProvider region provider} as a Gradle {@link BuildService build service}.
 * <p>
 *     The region is loaded from Gradle and system properties, similar to
 *     all AWS SDKs and AWS CLI.
 *     See the {@link #getAwsRegionProvider()} for details.
 * </p>
 * <h2>Example Usage</h2>
 * {@snippet lang=groovy file=fi/linuxbox/gradle/aws/regions/providers/GradlePropertyRegionProviderSpec.groovy region=javadoc-example}
 * <ol>
 *     <li>
 *         During build configuration,
 *         {@link #register(BuildServiceRegistry) register} this build service.
 *         From this registration, you receive the build service provider.
 *     </li>
 *     <li>
 *         Using the {@link org.gradle.api.Task#usesService(Provider) usesService} method,
 *         tell Gradle that the task uses this build service.
 *     </li>
 *     <li>
 *         During task execution, use the {@link Provider#get()} method to
 *         make use of the build service.  The AWS region provider will be
 *         created lazily when the {@code get()} method is first called.
 *     </li>
 * </ol>
 */
@NonNullApi
public abstract class AwsRegionProviderService implements BuildService<None> {
    private static final Logger log = LoggerFactory.getLogger(AwsRegionProviderService.class);

    /**
     * A name to use to identify this build service in the {@link BuildServiceRegistry}.
     * <p>
     *     When using the {@link #register(BuildServiceRegistry)} method,
     *     this is the name that is used in the
     *     invocation of {@link BuildServiceRegistry#registerIfAbsent(String, Class, Action)}.
     * </p>
     */
    public static final String NAME = "aws-region-provider-service";

    /**
     * Registers this build service, unless already registered.
     * <p>
     * The {@code sharedServices} registry is available using the
     * {@link org.gradle.api.invocation.Gradle#getSharedServices()} method.
     * You can obtain a Gradle instance by calling {@link Project#getGradle()}.
     * </p>
     *
     * @param sharedServices A registry of build services.
     * @return A provider that will create the service instance when queried.
     */
    public static Provider<AwsRegionProviderService> register(BuildServiceRegistry sharedServices) {
        log.debug("Registering AWS region provider service");
        return sharedServices.registerIfAbsent(
            AwsRegionProviderService.NAME,
            AwsRegionProviderService.class,
            (spec) -> {}
        );
    }

    /**
     * Returns the {@link AwsRegionProvider}.
     * <p>
     *     The region loading is delegated to
     *     {@link GradlePropertyRegionProvider} and
     *     {@link DefaultAwsRegionProviderChain}.  But in summary, the region
     *     is loaded from:
     * </p>
     * <ol>
     *      <li>Gradle property - {@systemProperty aws.region}</li>
     *      <li>Java System Property - {@systemProperty aws.region}</li>
     *      <li>Environment Variable - {@code AWS_REGION}</li>
     *      <li>Profiles files at the default location ({@code ~/.aws/credentials} and {@code ~/.aws/config}) shared by all AWS SDKs and the AWS CLI</li>
     *      <li>Region delivered through the Amazon EC2 metadata service</li>
     * </ol>
     * @return the {@link AwsRegionProvider}.
     */
    public AwsRegionProvider getAwsRegionProvider() {
        return awsRegionProviderChain;
    }

    /**
     * Injectable constructor.
     * <p>
     *     This is invoked by Gradle when this build service is needed by the
     *     build.
     * </p>
     *
     * @param providers A factory for creating instances of {@link Provider}.
     */
    @Inject
    public AwsRegionProviderService(ProviderFactory providers) {
        log.debug("Instantiating AWS region provider");
        awsRegionProviderChain = new AwsRegionProviderChain(
            new GradlePropertyRegionProvider(providers),
            new DefaultAwsRegionProviderChain()
        );
    }

    private final AwsRegionProviderChain awsRegionProviderChain;
}
