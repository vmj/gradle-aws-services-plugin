/**
 * Provides AWS STS client as a Gradle Build Service.
 */
package fi.linuxbox.gradle.aws.services.sts;