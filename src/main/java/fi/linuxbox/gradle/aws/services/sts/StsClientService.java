package fi.linuxbox.gradle.aws.services.sts;

import fi.linuxbox.gradle.aws.auth.credentials.AwsCredentialsProviderService;
import fi.linuxbox.gradle.aws.regions.providers.AwsRegionProviderService;
import fi.linuxbox.gradle.aws.services.AwsSyncClientService;
import org.gradle.api.Action;
import org.gradle.api.NonNullApi;
import org.gradle.api.Project;
import org.gradle.api.provider.Provider;
import org.gradle.api.services.BuildService;
import org.gradle.api.services.BuildServiceParameters.None;
import org.gradle.api.services.BuildServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.sts.StsClient;
import software.amazon.awssdk.services.sts.StsClientBuilder;

/**
 * Provides the {@link StsClient synchronous STS client} as a Gradle {@link BuildService build service}.
 * <h2>Example Usage</h2>
 * <p>
 *     Below is an example usage of this build service.  This code would go to
 *     your {@code build.gradle} file.
 * </p>
 * {@snippet lang=groovy file=fi/linuxbox/gradle/aws/services/sts/StsClientServiceSpec.groovy region=javadoc-example}
 * <ol>
 *     <li>
 *         In your <a href="https://docs.gradle.org/current/userguide/custom_gradle_types.html">custom Gradle type</a>,
 *         include an abstract getter for the build service provider.
 *         Gradle will implement this for you.
 *     </li>
 *     <li>
 *         During build configuration,
 *         {@link #register(BuildServiceRegistry) register} this build service.
 *         From this registration, you receive the build service provider.
 *     </li>
 *     <li>
 *         Assign the build service provider to the property of your custom type.
 *     </li>
 *     <li>
 *         Using the {@link org.gradle.api.Task#usesService(Provider) usesService} method,
 *         tell Gradle that the task uses this build service.
 *     </li>
 *     <li>
 *         During task execution, use the {@link Provider#get()} method to
 *         make use of the build service.  The AWS client will be created
 *         lazily when the {@code get()} method is first called.
 *     </li>
 * </ol>
 * <p>
 *     In a real world project, you would not hardcode the credentials in a
 *     properties file.  But for demo purposes, to make the above example work,
 *     you could add the following properties to your project
 *     {@code gradle.properties} file:
 * </p>
 * {@snippet lang=properties file=fi/linuxbox/gradle/aws/services/sts/StsClientServiceSpec.groovy region=region-and-credentials-properties}
 */
@NonNullApi
public abstract class StsClientService extends AwsSyncClientService<StsClientBuilder, StsClient> implements BuildService<None> {
    private static final Logger log = LoggerFactory.getLogger(StsClientService.class);

    /**
     * A name to use to identify this build service in the {@link BuildServiceRegistry}.
     * <p>
     *     When using the {@link #register(BuildServiceRegistry)} method,
     *     this is the name that is used in the
     *     invocation of {@link BuildServiceRegistry#registerIfAbsent(String, Class, Action)}.
     * </p>
     */
    public static final String NAME = "sts-client-service";

    /**
     * Registers this build service, unless already registered.
     * <p>
     * The {@code sharedServices} registry is available using the
     * {@link org.gradle.api.invocation.Gradle#getSharedServices()} method.
     * You can obtain a Gradle instance by calling {@link Project#getGradle()}.
     * </p>
     *
     * @param sharedServices A registry of build services.
     * @return A provider that will create the service instance when queried.
     */
    public static Provider<StsClientService> register(BuildServiceRegistry sharedServices) {
        log.debug("Registering AWS SDK client service '{}'", NAME);
        return sharedServices.registerIfAbsent(
            StsClientService.NAME,
            StsClientService.class,
            (spec) -> {}
        );
    }

    /**
     * Returns the synchronous STS client.
     * <p>
     *     The instantiation of the STS client requires that the AWS credentials
     *     and region are available.  This build service delegates to
     *     {@link AwsCredentialsProviderService} and {@link AwsRegionProviderService}
     *     for resolving those.
     * </p>
     * <p>
     *     For further, optional configuration, see the {@link #builder()} method,
     *     especially the base class implementations of it.
     * </p>
     *
     * @return the synchronous STS client
     */
    public StsClient getStsClient() {
        return client;
    }

    /**
     * Default constructor.
     * <p>
     *     This is invoked by Gradle when this build service is needed by the
     *     build.
     * </p>
     */
    public StsClientService() {
        super(NAME);
    }

    /**
     * Instantiate the synchronous STS client builder.
     *
     * @return The synchronous STS client builder
     */
    @Override
    protected StsClientBuilder createBuilder() {
        return StsClient.builder();
    }

    /**
     * Configures the synchronous STS client builder.
     * <p>
     *     Extends the {@link AwsSyncClientService#builder() base class implementation} by further configuring the builder:
     * </p>
     * <ul>
     *     <li>TODO: The endpoint is set using the {@link Object}</li>
     * </ul>
     * @return The configured client builder
     */
    @Override
    protected StsClientBuilder builder() {
        return super.builder()
            // StsBaseClientBuilder
//          .endpointProvider(stsEndpointProvider) // TODO
            ;
    }
}
