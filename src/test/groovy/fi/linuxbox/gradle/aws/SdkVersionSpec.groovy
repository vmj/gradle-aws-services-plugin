package fi.linuxbox.gradle.aws

import fi.linuxbox.gradle.GradleSpecification
import org.gradle.tooling.GradleConnector

class SdkVersionSpec extends GradleSpecification {

    void 'when used as a plugin, the build has AWS SDK as transitive dependency'() {
        given:
        settingsScript << """
        pluginManagement {
            includeBuild '${System.getProperty('projectUnderTestDir')}'
        }
        """

        and:
        buildScript << '''
        // tag::plugin-consumer-build-script[]
        plugins {
            id 'fi.linuxbox.awssdk' version '{gradle-project-version}'
        }
        // end::plugin-consumer-build-script[]
        '''

        and:
        final project = GradleConnector.newConnector()
                                       .forProjectDirectory(projectDir)
                                       .connect()

        and:
        final console = new ByteArrayOutputStream()

        and:
        final buildEnvironmentTask = project.newBuild()
                                            .setStandardOutput(console)
                                            .withArguments('-q', '--console=plain')
                                            .forTasks('buildEnvironment')

        when:
        buildEnvironmentTask.run()

        then:
        final output = console.toString()
        final expected = '''
        // tag::plugin-consumer-buildscriptClasspath[]
        classpath
        \\--- fi.linuxbox.awssdk:fi.linuxbox.awssdk.gradle.plugin:{gradle-project-version}
             \\--- fi.linuxbox.gradle:gradle-awssdk:{gradle-project-version}
                  +--- software.amazon.awssdk:bom:2.22.5
                  |    +--- software.amazon.awssdk:auth:2.22.5 (c)
                  |    +--- software.amazon.awssdk:regions:2.22.5 (c)
                  |    +--- software.amazon.awssdk:sdk-core:2.22.5 (c)
                  |    +--- software.amazon.awssdk:http-client-spi:2.22.5 (c)
                  |    +--- software.amazon.awssdk:apache-client:2.22.5 (c)
                  |    +--- software.amazon.awssdk:netty-nio-client:2.22.5 (c)
                  |    +--- software.amazon.awssdk:utils:2.22.5 (c)
                  |    +--- software.amazon.awssdk:annotations:2.22.5 (c)
                  |    +--- software.amazon.awssdk:identity-spi:2.22.5 (c)
                  |    +--- software.amazon.awssdk:profiles:2.22.5 (c)
                  |    +--- software.amazon.awssdk:json-utils:2.22.5 (c)
                  |    +--- software.amazon.awssdk:http-auth-aws:2.22.5 (c)
                  |    +--- software.amazon.awssdk:http-auth:2.22.5 (c)
                  |    +--- software.amazon.awssdk:http-auth-spi:2.22.5 (c)
                  |    +--- software.amazon.awssdk:metrics-spi:2.22.5 (c)
                  |    +--- software.amazon.awssdk:checksums-spi:2.22.5 (c)
                  |    +--- software.amazon.awssdk:checksums:2.22.5 (c)
                  |    \\--- software.amazon.awssdk:third-party-jackson-core:2.22.5 (c)
                  +--- software.amazon.awssdk:auth -> 2.22.5
        // end::plugin-consumer-buildscriptClasspath[]
        '''.split('\n').drop(5).dropRight(2).join('\n').stripIndent(13)

        output.contains expected
    }

    void 'the build author should be able to downgrade the AWS SDK used by the plugin'() {
        given:
        settingsScript << """
        pluginManagement {
            includeBuild '${System.getProperty('projectUnderTestDir')}'
        }
        """

        and:
        buildScript << '''
        // tag::plugin-consumer-build-script-downgrading-sdk-version[]
        buildscript {
            dependencies {
                classpath 'software.amazon.awssdk:bom', {
                    // Gradle would normally use the higher version number when multiple
                    // are specified.  When downgrading a version, strict version is
                    // required.
                    version {
                        strictly '2.21.0'
                    }
                }
            }
        }
        plugins {
            id 'fi.linuxbox.awssdk' version '{gradle-project-version}'
        }
        // end::plugin-consumer-build-script-downgrading-sdk-version[]
        '''

        and:
        final project = GradleConnector.newConnector()
                                       .forProjectDirectory(projectDir)
                                       .connect()

        and:
        final console = new ByteArrayOutputStream()

        and:
        final buildEnvironmentTask = project.newBuild()
                                        .setStandardOutput(console)
                                        .withArguments('-q', '--console=plain')
                                        .forTasks('buildEnvironment')

        when:
        buildEnvironmentTask.run()

        then:
        final output = console.toString()
        final expected = '''
        // tag::plugin-consumer-buildscriptClasspath-with-downgraded-sdk-version[]
        classpath
        +--- software.amazon.awssdk:bom:{strictly 2.21.0} -> 2.21.0
        |    +--- software.amazon.awssdk:auth:2.21.0 (c)
        |    +--- software.amazon.awssdk:regions:2.21.0 (c)
        |    +--- software.amazon.awssdk:sdk-core:2.21.0 (c)
        |    +--- software.amazon.awssdk:http-client-spi:2.21.0 (c)
        |    +--- software.amazon.awssdk:apache-client:2.21.0 (c)
        |    +--- software.amazon.awssdk:netty-nio-client:2.21.0 (c)
        |    +--- software.amazon.awssdk:utils:2.21.0 (c)
        |    +--- software.amazon.awssdk:annotations:2.21.0 (c)
        |    +--- software.amazon.awssdk:identity-spi:2.21.0 (c)
        |    +--- software.amazon.awssdk:profiles:2.21.0 (c)
        |    +--- software.amazon.awssdk:json-utils:2.21.0 (c)
        |    +--- software.amazon.awssdk:http-auth-aws:2.21.0 (c)
        |    +--- software.amazon.awssdk:http-auth:2.21.0 (c)
        |    +--- software.amazon.awssdk:http-auth-spi:2.21.0 (c)
        |    +--- software.amazon.awssdk:metrics-spi:2.21.0 (c)
        |    +--- software.amazon.awssdk:checksums-spi:2.21.0 (c)
        |    +--- software.amazon.awssdk:checksums:2.21.0 (c)
        |    \\--- software.amazon.awssdk:third-party-jackson-core:2.21.0 (c)
        \\--- fi.linuxbox.awssdk:fi.linuxbox.awssdk.gradle.plugin:{gradle-project-version}
             \\--- fi.linuxbox.gradle:gradle-awssdk:{gradle-project-version}
                  +--- software.amazon.awssdk:bom:2.22.5 -> 2.21.0 (*)
                  +--- software.amazon.awssdk:auth -> 2.21.0
        // end::plugin-consumer-buildscriptClasspath-with-downgraded-sdk-version[]
        '''
        final expected1 = expected.split('\n').drop(2).dropRight(6).join('\n').stripIndent(8)
        final expected2 = expected.split('\n').drop(24).dropRight(2).join('\n').stripIndent(13)
        output.contains expected1
        output.contains expected2
    }

    void 'when used as a library, the build has AWS SDK as transitive dependency'() {
        given:
        settingsScript << """
        includeBuild '${System.getProperty('projectUnderTestDir')}'
        """

        and:
        buildScript << '''
        // tag::library-consumer-build-script[]
        plugins {
            id 'java-library'
        }
        
        repositories {
            mavenCentral()
        }
        
        dependencies {
            implementation 'fi.linuxbox.gradle:gradle-awssdk:{gradle-project-version}'
        }
        // end::library-consumer-build-script[]
        '''

        and:
        final project = GradleConnector.newConnector()
                                       .forProjectDirectory(projectDir)
                                       .connect()

        and:
        final console = new ByteArrayOutputStream()

        and:
        final dependenciesTask = project.newBuild()
                                        .setStandardOutput(console)
                                        .withArguments('-q', '--console=plain')
                                        .forTasks('dependencies', '--configuration=runtimeClasspath')

        when:
        dependenciesTask.run()

        then:
        final output = console.toString()
        final expected = '''
        // tag::library-consumer-runtimeClasspath[]
        runtimeClasspath - Runtime classpath of source set 'main'.
        \\--- fi.linuxbox.gradle:gradle-awssdk:{gradle-project-version}
             +--- software.amazon.awssdk:bom:2.22.5
             |    +--- software.amazon.awssdk:auth:2.22.5 (c)
             |    +--- software.amazon.awssdk:regions:2.22.5 (c)
             |    +--- software.amazon.awssdk:sdk-core:2.22.5 (c)
             |    +--- software.amazon.awssdk:http-client-spi:2.22.5 (c)
             |    +--- software.amazon.awssdk:apache-client:2.22.5 (c)
             |    +--- software.amazon.awssdk:netty-nio-client:2.22.5 (c)
             |    +--- software.amazon.awssdk:utils:2.22.5 (c)
             |    +--- software.amazon.awssdk:annotations:2.22.5 (c)
             |    +--- software.amazon.awssdk:identity-spi:2.22.5 (c)
             |    +--- software.amazon.awssdk:profiles:2.22.5 (c)
             |    +--- software.amazon.awssdk:json-utils:2.22.5 (c)
             |    +--- software.amazon.awssdk:http-auth-aws:2.22.5 (c)
             |    +--- software.amazon.awssdk:http-auth:2.22.5 (c)
             |    +--- software.amazon.awssdk:http-auth-spi:2.22.5 (c)
             |    +--- software.amazon.awssdk:metrics-spi:2.22.5 (c)
             |    +--- software.amazon.awssdk:checksums-spi:2.22.5 (c)
             |    +--- software.amazon.awssdk:checksums:2.22.5 (c)
             |    \\--- software.amazon.awssdk:third-party-jackson-core:2.22.5 (c)
             +--- software.amazon.awssdk:auth -> 2.22.5
        // end::library-consumer-runtimeClasspath[]
        '''.split('\n').drop(4).dropRight(2).join('\n').stripIndent(8)

        output.contains expected
    }

    void 'the build author should be able to downgrade the AWS SDK used by the library'() {
        given:
        settingsScript << """
        includeBuild '${System.getProperty('projectUnderTestDir')}'
        """

        and:
        buildScript << '''
        // tag::library-consumer-build-script-downgrading-sdk-version[]
        plugins {
            id 'java-library'
        }
        
        repositories {
            mavenCentral()
        }
        
        dependencies {
            implementation platform('software.amazon.awssdk:bom'), {
                // Gradle would normally use the higher version number when multiple
                // are specified.  When downgrading a version, strict version is
                // required.
                version {
                    strictly '2.21.0'
                }
            }
            implementation 'fi.linuxbox.gradle:gradle-awssdk:{gradle-project-version}'
        }
        // end::library-consumer-build-script-downgrading-sdk-version[]
        '''

        and:
        final project = GradleConnector.newConnector()
                                       .forProjectDirectory(projectDir)
                                       .connect()

        and:
        final console = new ByteArrayOutputStream()

        and:
        final dependenciesTask = project.newBuild()
                                        .setStandardOutput(console)
                                        .withArguments('-q', '--console=plain')
                                        .forTasks('dependencies', '--configuration=runtimeClasspath')

        when:
        dependenciesTask.run()

        then:
        final output = console.toString()
        final expected = '''
        // tag::library-consumer-runtimeClasspath-with-downgraded-sdk-version[]
        runtimeClasspath - Runtime classpath of source set 'main'.
        +--- software.amazon.awssdk:bom:{strictly 2.21.0} -> 2.21.0
        |    +--- software.amazon.awssdk:auth:2.21.0 (c)
        |    +--- software.amazon.awssdk:http-client-spi:2.21.0 (c)
        |    +--- software.amazon.awssdk:regions:2.21.0 (c)
        |    +--- software.amazon.awssdk:sdk-core:2.21.0 (c)
        |    +--- software.amazon.awssdk:utils:2.21.0 (c)
        |    +--- software.amazon.awssdk:apache-client:2.21.0 (c)
        |    +--- software.amazon.awssdk:netty-nio-client:2.21.0 (c)
        |    +--- software.amazon.awssdk:annotations:2.21.0 (c)
        |    +--- software.amazon.awssdk:identity-spi:2.21.0 (c)
        |    +--- software.amazon.awssdk:profiles:2.21.0 (c)
        |    +--- software.amazon.awssdk:json-utils:2.21.0 (c)
        |    +--- software.amazon.awssdk:http-auth-aws:2.21.0 (c)
        |    +--- software.amazon.awssdk:http-auth:2.21.0 (c)
        |    +--- software.amazon.awssdk:http-auth-spi:2.21.0 (c)
        |    +--- software.amazon.awssdk:metrics-spi:2.21.0 (c)
        |    +--- software.amazon.awssdk:checksums-spi:2.21.0 (c)
        |    +--- software.amazon.awssdk:checksums:2.21.0 (c)
        |    \\--- software.amazon.awssdk:third-party-jackson-core:2.21.0 (c)
        \\--- fi.linuxbox.gradle:gradle-awssdk:{gradle-project-version}
             +--- software.amazon.awssdk:bom:2.22.5 -> 2.21.0 (*)
             +--- software.amazon.awssdk:auth -> 2.21.0
        // end::library-consumer-runtimeClasspath-with-downgraded-sdk-version[]
        '''

        final expected1 = expected.split('\n').drop(2).dropRight(5).join('\n').stripIndent()
        final expected2 = expected.split('\n').drop(23).dropRight(2).join('\n').stripIndent(8)

        output.contains expected1
        output.contains expected2
    }

}
