package fi.linuxbox.gradle.aws.http

import fi.linuxbox.gradle.GradleSpecification
import org.gradle.tooling.GradleConnector

class SdkHttpClientSpec extends GradleSpecification {

    void 'the plugin should use Apache-based HTTP client by default'() {
        given:
        buildScript << '''
        // tag::example[] // @start region=javadoc-example
        plugins {
            id 'fi.linuxbox.awssdk' version '{gradle-project-version}'
        }

        import fi.linuxbox.gradle.aws.http.SdkHttpClientService

        final buildService = SdkHttpClientService.register(project.gradle.sharedServices) // <1>

        tasks.register('printHttpClientName') {
            usesService buildService // <2>

            doLast {
                final httpClient = buildService.get().sdkHttpClient // <3>
                println httpClient.clientName()
            }
        }
        // end::example[] // @end region=javadoc-example
        '''

        when:
        final result = gradle(latestGradleVersion, 'printHttpClientName').build()

        then:
        result.output.contains 'Apache'
    }

    void 'build author should be able to use UrlConnection-based HTTP client in their build'() {
        given:
        settingsScript << """
        pluginManagement {
            includeBuild '${System.getProperty('projectUnderTestDir')}'
        }
        """

        and:
        gradleProperties << '''
        // tag::plugin-consumer-gradle-properties[]
        software.amazon.awssdk.http.service.impl = software.amazon.awssdk.http.urlconnection.UrlConnectionSdkHttpService
        // end::plugin-consumer-gradle-properties[]
        '''

        and:
        buildScript << '''
        // tag::plugin-consumer-build-gradle[]
        buildscript {
            dependencies {
                classpath 'software.amazon.awssdk:url-connection-client'
            }
        }
        plugins {
            id 'fi.linuxbox.awssdk' version '{gradle-project-version}'
        }
        // end::plugin-consumer-build-gradle[]

        // tag::plugin-consumer-build-gradle-example[]
        import fi.linuxbox.gradle.aws.http.SdkHttpClientService

        final buildService = SdkHttpClientService.register(project.gradle.sharedServices)

        tasks.register('printHttpClientName') {
            usesService buildService

            doLast {
                final httpClient = buildService.get().sdkHttpClient
                print httpClient.clientName()
            }
        }
        // end::plugin-consumer-build-gradle-example[]
        '''

        and:
        final project = GradleConnector.newConnector()
                                       .forProjectDirectory(projectDir)
                                       .connect()

        and:
        final console = new ByteArrayOutputStream()

        and:
        final task = project.newBuild()
                            .setStandardOutput(console)
                            .withArguments('-q', '--console=plain')
                            .forTasks('printHttpClientName')

        when:
        task.run()

        then:
        console.toString() == 'UrlConnection'
    }

    void 'the library should use Apache-based HTTP client by default'() {
        given:
        settingsScript << """
        includeBuild '${System.getProperty('projectUnderTestDir')}'
        """

        and:
        buildScript << '''
        // tag::library-consumer-build-gradle[]
        plugins {
            id 'java-library'
            id 'application'
        }

        repositories {
            mavenCentral()
        }

        application {
            mainClass = 'app.Main'
        }

        dependencies {
            implementation 'fi.linuxbox.gradle:gradle-awssdk:{gradle-project-version}'
        }
        // end::library-consumer-build-gradle[]
        '''

        and:
        projectFile('src/main/java/app/Main.java') << '''
        // tag::library-consumer-main-java[]
        package app;

        import fi.linuxbox.gradle.aws.http.SdkHttpClientService;
        import software.amazon.awssdk.http.SdkHttpClient;

        import org.gradle.api.Project;
        import org.gradle.api.provider.Provider;
        import org.gradle.api.services.BuildServiceRegistry;
        import org.gradle.testfixtures.ProjectBuilder;

        public final class Main {
            public static void main(String... args) {
                final Project project = ProjectBuilder.builder().build();
                final BuildServiceRegistry sharedServices = project.getGradle().getSharedServices();

                final Provider<SdkHttpClientService> serviceProvider = SdkHttpClientService.register(sharedServices);

                final SdkHttpClient sdkHttpClient = serviceProvider.get().getSdkHttpClient();

                System.out.println(sdkHttpClient.clientName());
            }
        }
        // end::library-consumer-main-java[]
        '''

        and:
        final project = GradleConnector.newConnector()
                                       .forProjectDirectory(projectDir)
                                       .connect()

        and:
        final console = new ByteArrayOutputStream()

        and:
        final task = project.newBuild()
                            .setStandardOutput(console)
                            .withArguments('-q', '--console=plain')
                            .forTasks('run')

        when:
        task.run()

        then:
        console.toString() == 'Apache\n'
    }

    void 'the library consumer should be able to use UrlConnection-based HTTP client by changing the dependencies'() {
        given:
        settingsScript << """
        includeBuild '${System.getProperty('projectUnderTestDir')}'
        """

        and:
        buildScript << '''
        plugins {
            id 'java-library'
            id 'application'
        }

        repositories {
            mavenCentral()
        }

        application {
            mainClass = 'app.Main'
        }

        // tag::library-consumer-url-connection-dependencies[]
        dependencies {
            implementation 'fi.linuxbox.gradle:gradle-awssdk:{gradle-project-version}', {
                exclude group: 'software.amazon.awssdk', module: 'apache-client'
            }
            runtimeOnly 'software.amazon.awssdk:url-connection-client'
        }
        // end::library-consumer-url-connection-dependencies[]
        '''

        and:
        projectFile('src/main/java/app/Main.java') << '''
        package app;

        import fi.linuxbox.gradle.aws.http.SdkHttpClientService;
        import software.amazon.awssdk.http.SdkHttpClient;

        import org.gradle.api.Project;
        import org.gradle.api.provider.Provider;
        import org.gradle.api.services.BuildServiceRegistry;
        import org.gradle.testfixtures.ProjectBuilder;

        public final class Main {
            public static void main(String... args) {
                final Project project = ProjectBuilder.builder().build();
                final BuildServiceRegistry sharedServices = project.getGradle().getSharedServices();

                final Provider<SdkHttpClientService> serviceProvider = SdkHttpClientService.register(sharedServices);

                final SdkHttpClient sdkHttpClient = serviceProvider.get().getSdkHttpClient();

                System.out.println(sdkHttpClient.clientName());
            }
        }
        '''

        and:
        final project = GradleConnector.newConnector()
                                       .forProjectDirectory(projectDir)
                                       .connect()

        and:
        final console = new ByteArrayOutputStream()

        and:
        final task = project.newBuild()
                            .setStandardOutput(console)
                            .withArguments('-q', '--console=plain')
                            .forTasks('run')

        when:
        task.run()

        then:
        console.toString() == 'UrlConnection\n'
    }
}
