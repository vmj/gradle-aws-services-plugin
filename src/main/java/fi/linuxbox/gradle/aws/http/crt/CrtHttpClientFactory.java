package fi.linuxbox.gradle.aws.http.crt;

import fi.linuxbox.gradle.aws.utils.DurationParser;
import fi.linuxbox.gradle.aws.utils.PropertyApplier;
import fi.linuxbox.gradle.aws.http.SdkAsyncHttpClientService;
import org.gradle.api.NonNullApi;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import software.amazon.awssdk.http.SdkHttpClient;
import software.amazon.awssdk.http.async.SdkAsyncHttpClient;
import software.amazon.awssdk.http.crt.AwsCrtAsyncHttpClient;
import software.amazon.awssdk.http.crt.ConnectionHealthConfiguration;
import software.amazon.awssdk.http.crt.ProxyConfiguration;
import software.amazon.awssdk.http.crt.TcpKeepAliveConfiguration;

import javax.inject.Inject;
import java.time.Duration;
import java.util.function.Supplier;

/**
 * Factory class for CRT-based HTTP client.
 * <p>
 * This configures the client from Gradle and system properties.
 * </p>
 * <p>
 *     This is not typically used directly, but via
 *     {@link SdkAsyncHttpClientService}.
 * </p>
 */
@NonNullApi
abstract public class CrtHttpClientFactory implements Supplier<SdkAsyncHttpClient> {
    /**
     * Gradle and system property name prefix for CRT related
     * settings.
     */
    public final static String CRT_PREFIX = "software.amazon.awssdk.http.crt.";

    private final static String CONNECTION_TIMEOUT = CRT_PREFIX + "connection.timeout";
    private final static String CONNECTION_MAX_IDLE_TIME = CRT_PREFIX + "connection.max.idle.time";
    private final static String MAX_CONCURRENCY = CRT_PREFIX + "max.concurrency";
    private final static String TCP_KEEP_ALIVE_INTERVAL = CRT_PREFIX + "tcp.keep.alive.interval";
    private final static String TCP_KEEP_ALIVE_TIMEOUT = CRT_PREFIX + "tcp.keep.alive.timeout";
    private final static String CONNECTION_HEALTH_MINIMUM_THROUGHPUT_IN_BPS = "connection.health.minimum.throughput.in.bps";
    private final static String CONNECTION_HEALTH_MINIMUM_THROUGHPUT_TIMEOUT = "connection.health.minimum.throughput.timeout";
    private final static String POST_QUANTUM_TLS_ENABLED = "post.quantum.tls.enabled";
    private final static String READ_BUFFER_SIZE_IN_BYTES = "read.buffer.size.in.bytes";

    private final PropertyApplier crtProperties;

    /**
     * Injectable constructor.
     *
     * @param providers A factory for creating instances of {@link Provider}.
     */
    @Inject
    public CrtHttpClientFactory(ProviderFactory providers) {
        crtProperties = new PropertyApplier(providers);
    }

    /**
     * Factory method for CRT-based HTTP client.
     * <p>
     * The following Gradle and system properties are considered:
     * </p>
     * <dl>
     *     <dt>{@value CONNECTION_TIMEOUT}</dt>
     *     <dd>
     *     Sets the {@link AwsCrtAsyncHttpClient.Builder#connectionTimeout(Duration) connection timeout}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value CONNECTION_MAX_IDLE_TIME}</dt>
     *     <dd>
     *     Sets the {@link AwsCrtAsyncHttpClient.Builder#connectionMaxIdleTime(Duration) max connection idle time}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value MAX_CONCURRENCY}</dt>
     *     <dd>
     *     Sets the {@link AwsCrtAsyncHttpClient.Builder#maxConcurrency(Integer) max number of concurrent requests}.
     *     See {@link Integer#parseInt(String) int formats} for valid values.
     *     </dd>
     *     <dt>{@value TCP_KEEP_ALIVE_INTERVAL}</dt>
     *     <dd>
     *     Sets the {@link TcpKeepAliveConfiguration.Builder#keepAliveInterval(Duration) TCP KeepAlive interval}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value TCP_KEEP_ALIVE_TIMEOUT}</dt>
     *     <dd>
     *     Sets the {@link TcpKeepAliveConfiguration.Builder#keepAliveTimeout(Duration) TCP KeepAlive timeout}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value CONNECTION_HEALTH_MINIMUM_THROUGHPUT_IN_BPS}</dt>
     *     <dd>
     *     Sets the {@link ConnectionHealthConfiguration.Builder#minimumThroughputInBps(Long) throughput threshold for connections}.
     *     See {@link Long#parseLong(String) long formats} for valid values.
     *     </dd>
     *     <dt>{@value CONNECTION_HEALTH_MINIMUM_THROUGHPUT_TIMEOUT}</dt>
     *     <dd>
     *     Sets the {@link ConnectionHealthConfiguration.Builder#minimumThroughputTimeout(Duration) timeout for unhealthy connections}.
     *     See {@link DurationParser#parse(String) duration formats} for valid values.
     *     </dd>
     *     <dt>{@value POST_QUANTUM_TLS_ENABLED}</dt>
     *     <dd>
     *     Sets whether to use the {@link AwsCrtAsyncHttpClient.Builder#postQuantumTlsEnabled(Boolean) post-quantum key exchange option for TLS}.
     *     See {@link Boolean#valueOf(String) boolean formats} for valid values.
     *     </dd>
     *     <dt>{@value READ_BUFFER_SIZE_IN_BYTES}</dt>
     *     <dd>
     *     Sets the {@link AwsCrtAsyncHttpClient.Builder#readBufferSizeInBytes(Long) unread bytes that can be buffered}.
     *     See {@link Long#parseLong(String) long formats} for valid values.
     *     </dd>
     * </dl>
     *
     * @return An {@link SdkHttpClient} based on {@link AwsCrtAsyncHttpClient}.
     */
    @Override
    public SdkAsyncHttpClient get() {
        final AwsCrtAsyncHttpClient.Builder builder = AwsCrtAsyncHttpClient.builder();
        final ProxyConfiguration.Builder proxy = ProxyConfiguration.builder();
        final TcpKeepAliveConfiguration.Builder tcpKeepAlive = TcpKeepAliveConfiguration.builder();
        final ConnectionHealthConfiguration.Builder connectionHealth = ConnectionHealthConfiguration.builder();

        crtProperties.gradleOrSystemProperty(CONNECTION_TIMEOUT,
                                             Duration::parse,
                                             builder::connectionTimeout);

        crtProperties.gradleOrSystemProperty(CONNECTION_MAX_IDLE_TIME,
                                             DurationParser::parse,
                                             builder::connectionMaxIdleTime);

        crtProperties.gradleOrSystemProperty(MAX_CONCURRENCY,
                                             Integer::parseInt,
                                             builder::maxConcurrency);


        crtProperties.gradleOrSystemProperty(TCP_KEEP_ALIVE_INTERVAL,
                                             DurationParser::parse,
                                             tcpKeepAlive::keepAliveInterval);
        crtProperties.gradleOrSystemProperty(TCP_KEEP_ALIVE_TIMEOUT,
                                             DurationParser::parse,
                                             tcpKeepAlive::keepAliveTimeout);
        try {
            builder.tcpKeepAliveConfiguration(tcpKeepAlive.build());
        } catch (IllegalArgumentException e) {

        }

        crtProperties.gradleOrSystemProperty(CONNECTION_HEALTH_MINIMUM_THROUGHPUT_IN_BPS,
                                             Long::parseLong,
                                             connectionHealth::minimumThroughputInBps);
        crtProperties.gradleOrSystemProperty(CONNECTION_HEALTH_MINIMUM_THROUGHPUT_TIMEOUT,
                                             DurationParser::parse,
                                             connectionHealth::minimumThroughputTimeout);
        try {
            builder.connectionHealthConfiguration(connectionHealth.build());
        } catch (NullPointerException e) {

        }

        crtProperties.gradleOrSystemProperty(POST_QUANTUM_TLS_ENABLED,
                                             Boolean::valueOf,
                                             builder::postQuantumTlsEnabled);

        crtProperties.gradleOrSystemProperty(READ_BUFFER_SIZE_IN_BYTES,
                                             Long::valueOf,
                                             builder::readBufferSizeInBytes);

        builder.proxyConfiguration(proxy.build());
        return builder.build();
    }
}
