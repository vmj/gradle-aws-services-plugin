package fi.linuxbox.gradle.aws.utils


import org.gradle.api.provider.Property
import org.gradle.api.provider.ProviderFactory
import spock.lang.Specification
import spock.lang.Subject

class PropertyApplierSpec extends Specification {

    ProviderFactory providers = Mock()

    @Subject
    PropertyApplier props = new PropertyApplier(providers)

    void 'it should find a Gradle property'() {
        given: 'a property name and the expected value'
        final propertyName = 'foo'
        final expectedValue = 'some-value'

        and: 'a fake Gradle property'
        final property = fakeProperty(expectedValue)
        providers.gradleProperty(propertyName) >> property

        when:
        String actualValue = null
        props.gradleProperty(propertyName, value -> actualValue = value)

        then:
        actualValue == expectedValue
    }

    void 'it should transform a Gradle property'() {
        given: 'a property name and the expected value'
        final propertyName = 'foo'
        final expectedValue = 'true'

        and: 'a fake Gradle property'
        final property = fakeProperty(expectedValue)
        providers.gradleProperty(propertyName) >> property

        when:
        boolean actualValue = false
        props.gradleProperty(propertyName,
                             Boolean::valueOf,
                             value -> actualValue = value)

        then:
        actualValue === true
    }

    void 'it should not confuse Gradle property to a system property'() {
        given: 'a property name and the expected value'
        final propertyName = 'foo'
        final expectedValue = 'some-value'

        and: 'a fake system property'
        final property = fakeProperty(expectedValue)
        providers.gradleProperty(propertyName) >> missingProperty()
        providers.systemProperty(propertyName) >> property

        when:
        String actualValue = null
        props.gradleProperty(propertyName, value -> actualValue = value)

        then:
        actualValue == null
    }

    void 'it should prefer Gradle property over system property'() {
        given: 'a property name and the expected value'
        final propertyName = 'foo'
        final expectedValue = 'some-value'

        and: 'a fake system property'
        final property = fakeProperty(expectedValue)
        providers.gradleProperty(propertyName) >> property
        providers.systemProperty(propertyName) >> fakeProperty('wrong-value')

        when:
        String actualValue = null
        props.gradleOrSystemProperty(propertyName, value -> actualValue = value)

        then:
        actualValue == expectedValue
    }

    void 'it should find a system property'() {
        given: 'a property name and the expected value'
        final propertyName = 'foo'
        final expectedValue = 'some-value'

        and: 'a fake system property'
        final property = fakeProperty(expectedValue)
        providers.systemProperty(propertyName) >> property

        when:
        String actualValue = null
        props.systemProperty(propertyName, value -> actualValue = value)

        then:
        actualValue == expectedValue
    }

    void 'it should transform a system property'() {
        given: 'a property name and the expected value'
        final propertyName = 'foo'
        final expectedValue = 'true'

        and: 'a fake Gradle property'
        final property = fakeProperty(expectedValue)
        providers.systemProperty(propertyName) >> property

        when:
        boolean actualValue = false
        props.systemProperty(propertyName,
                             Boolean::valueOf,
                             value -> actualValue = value)

        then:
        actualValue === true
    }

    void 'it should not confuse system property to a Gradle property'() {
        given: 'a property name and the expected value'
        final propertyName = 'foo'
        final expectedValue = 'some-value'

        and: 'a fake Gradle property'
        final property = fakeProperty(expectedValue)
        providers.gradleProperty(propertyName) >> property
        providers.systemProperty(propertyName) >> missingProperty()

        when:
        String actualValue = null
        props.systemProperty(propertyName, value -> actualValue = value)

        then:
        actualValue == null
    }

    private Property<String> fakeProperty(String value) {
        final Property<String> property = Mock()
        property.isPresent() >> true
        property.orElse(_) >> property
        property.map(_) >> { args ->
            value = args[0].transform(value)
            return property
        }
        property.get() >> value
        return property
    }

    private Property<String> missingProperty() {
        final Property<String> property = Mock()
        property.isPresent() >> false
        return property
    }
}
