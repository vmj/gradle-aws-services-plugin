/**
 * Gradle additions to AWS region loading.
 */
package fi.linuxbox.gradle.aws.regions.providers;
