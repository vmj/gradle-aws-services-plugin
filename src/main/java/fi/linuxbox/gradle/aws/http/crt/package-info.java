/**
 * CRT-based HTTP client factory that configures it from Gradle and system properties.
 * <p>
 *     This is not typically used directly, but via
 *     {@link fi.linuxbox.gradle.aws.http.SdkAsyncHttpClientService}.
 * </p>
 */
package fi.linuxbox.gradle.aws.http.crt;
