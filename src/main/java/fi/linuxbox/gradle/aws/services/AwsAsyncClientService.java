package fi.linuxbox.gradle.aws.services;

import fi.linuxbox.gradle.aws.http.SdkAsyncHttpClientService;
import org.gradle.api.NonNullApi;
import org.gradle.api.tasks.Nested;
import software.amazon.awssdk.awscore.client.builder.AwsClientBuilder;
import software.amazon.awssdk.core.client.builder.SdkAsyncClientBuilder;
import software.amazon.awssdk.utils.SdkAutoCloseable;

/**
 * Base class for build services for asynchronous AWS SDK clients.
 *
 * @param <B> Type of the asynchronous client builder
 * @param <C> Type of the asynchronous client
 */
@NonNullApi
public abstract class AwsAsyncClientService<B extends SdkAsyncClientBuilder<B, C> & AwsClientBuilder<B, C>, C extends SdkAutoCloseable> extends AwsClientService<B, C> {

    /**
     * Constructor.
     *
     * @param name The name to show while logging the client lifecycle
     */
    protected AwsAsyncClientService(final String name) {
        super(name);
    }

    /**
     * Configures the AWS SDK client builder.
     * <p>
     *     Extends the {@link AwsClientService#builder() base class implementation} by further configuring the AWS SDK client builder:
     * </p>
     * <ul>
     *     <li>The asynchronous SDK HTTP client is set using the {@link SdkAsyncHttpClientService}</li>
     * </ul>
     * <p>
     *     Concrete subclasses, if overriding this method, should base call.
     * </p>
     * @return The configured client builder
     */
    @Override
    protected B builder() {
        return super.builder()
             .httpClient(getSdkAsyncHttpClientService().getSdkAsyncHttpClient());
    }

    /**
     * Gradle generated getter for asynchronous HTTP client provider.
     *
     * @return the asynchronous HTTP client provider
     */
    @Nested
    protected abstract SdkAsyncHttpClientService getSdkAsyncHttpClientService();

}
